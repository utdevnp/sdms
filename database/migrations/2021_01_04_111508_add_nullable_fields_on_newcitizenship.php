<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableFieldsOnNewcitizenship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            $table->string("mname_eng",254)->nullable()->change();
            $table->string("mname_nep",254)->nullable()->change();
            $table->string("father_m_name")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
