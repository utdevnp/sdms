<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableCitienshipChangeAddUserid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasColumn("citizenship_changes","user_id")){
                Schema::table('citizenship_changes', function (Blueprint $table) {
                    $table->bigInteger("user_id");
                    $table->bigInteger("branch_id")->nullable();
                });
        }
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citizenship_changes', function (Blueprint $table) {
            //
        });
    }
}
