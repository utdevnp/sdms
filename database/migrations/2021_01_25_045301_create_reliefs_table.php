<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReliefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable("reliefs")){
            Schema::create('reliefs', function (Blueprint $table) {
                $table->id();
                $table->string("full_name");
                $table->string("accident_case");
                $table->string("accident_date");
                $table->bigInteger("district_id")->nullable();
                $table->bigInteger("locallevel_id")->nullable();
                $table->bigInteger("ward_no")->nullable();
                $table->longText("street_address");
                $table->string("mobile_number");
                $table->string("bank_acc");
                $table->string("bank_name");
                $table->bigInteger("user_id");
                $table->bigInteger("branch_id")->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reliefs');
    }
}
