<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExarmiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable("exarmies")){
            Schema::create('exarmies', function (Blueprint $table) {
                $table->id();
                $table->string("name");
                $table->bigInteger("district_id");
                $table->bigInteger("local_level_id");
                $table->string("citizenship_id")->nullable();
                $table->string("woda")->nullable();
                $table->string("street")->nullable();

                $table->string("relation")->nullable();
                $table->string("patta_id")->nullable();
                $table->string("ex_army_name")->nullable();
                $table->string("pass_away_date")->nullable();
                $table->string("famaily_mamber")->nullable();

                $table->string("contact_number")->nullable();
                $table->bigInteger("user_id");
                $table->bigInteger("branch_id");

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exarmies');
    }
}
