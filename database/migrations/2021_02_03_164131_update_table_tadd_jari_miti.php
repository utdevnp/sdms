<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableTaddJariMiti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(! Schema::hasColumn("e_forigen_emps","type")){ 
            Schema::table('e_forigen_emps', function (Blueprint $table) {
                $table->string("type")->nullable();
                $table->string("citizenship_issue_date")->nullable();
                $table->string("death_report_id")->nullable();
                $table->string("passport_issue_date")->nullable();

            });
         }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('e_forigen_emps', function (Blueprint $table) {
            //
        });
    }
}
