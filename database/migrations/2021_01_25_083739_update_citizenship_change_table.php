<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCitizenshipChangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   if(! Schema::hasColumn("citizenship_changes","status")){
            Schema::table('citizenship_changes', function (Blueprint $table) {
                $table->enum("status",['pending',"verified","rejected"])->default("pending");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citizenship_changes', function (Blueprint $table) {
            //
        });
    }
}
