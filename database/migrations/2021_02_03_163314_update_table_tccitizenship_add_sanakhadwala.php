<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableTccitizenshipAddSanakhadwala extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasColumn("newcitizenships","proof_name")){
            Schema::table('newcitizenships', function (Blueprint $table) {
                $table->string("proof_name")->nullable();
                $table->string("proof_citizen_id")->nullable();
                $table->string("proof_relation")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
