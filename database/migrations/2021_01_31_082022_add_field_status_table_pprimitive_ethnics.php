<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldStatusTablePprimitiveEthnics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        if(!Schema::hasColumn('primitive_ethnics',"status")) {
            Schema::table('primitive_ethnics', function (Blueprint $table) {
                $table->enum('status', array('pending','verified','rejected'))->default("pending");
            });
    }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('primitive_ethnics', function (Blueprint $table) {
            //
        });
    }
}
