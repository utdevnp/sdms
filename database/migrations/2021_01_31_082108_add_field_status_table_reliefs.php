<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldStatusTableReliefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       

        if(!Schema::hasColumn('reliefs',"status")) {
            Schema::table('reliefs', function (Blueprint $table) {
                $table->enum('status', array('pending','verified','rejected'))->default("pending");
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reliefs', function (Blueprint $table) {
            //
        });
    }
}
