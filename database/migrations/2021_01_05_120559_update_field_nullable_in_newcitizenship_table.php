<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldNullableInNewcitizenshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            $table->string("father_l_name")->nullable()->change();
            $table->string("father_f_name")->nullable()->change();
            $table->string("dobin_eng")->nullable()->change();
            $table->string("dob_district")->nullable()->change();
            $table->string("dobin_nep")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
