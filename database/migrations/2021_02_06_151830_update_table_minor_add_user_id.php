<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableMinorAddUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn("minors","user_id")){
            Schema::table('minors', function (Blueprint $table) {
                $table->bigInteger("user_id")->nullable();
                $table->bigInteger("branch_id")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minors', function (Blueprint $table) {
            //
        });
    }
}
