<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldStatusTableExarmies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        if(!Schema::hasColumn('exarmies',"status")) {
            Schema::table('exarmies', function (Blueprint $table) {
                $table->enum('status', array('pending','verified','rejected'))->default("pending");
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exarmies', function (Blueprint $table) {
            //
        });
    }
}
