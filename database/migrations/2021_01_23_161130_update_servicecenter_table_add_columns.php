<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServicecenterTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('service_centers', 'officer_name')) {
            Schema::table('service_centers', function (Blueprint $table) {
                $table->string("postal_code")->nullable()->change();
                $table->string("officer_name")->nullable();
                $table->string("officer_phone")->nullable();
                $table->string("position")->nullable();
                $table->bigInteger("user_id")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('service_centers', function (Blueprint $table) {
            
        });
       
    }
}
