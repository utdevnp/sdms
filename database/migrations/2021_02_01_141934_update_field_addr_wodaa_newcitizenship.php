<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldAddrWodaaNewcitizenship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn("newcitizenships","addr_wodano")){
            Schema::table('newcitizenships', function (Blueprint $table) {
                $table->string("addr_wodano")->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
