<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsCitizenshipAddDobIdnoEtc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasColumn("newcitizenships","birth_id_no")){
            Schema::table('newcitizenships', function (Blueprint $table) {
                $table->string("birth_id_no")->nullable();
                $table->string("birth_issue_date")->nullable();
                $table->string("certificate_name")->nullable();
                $table->string("certificate_dob")->nullable();
                $table->string("migration_issue_date")->nullable();
                $table->string("migration_issue_number")->nullable();
                $table->string("postal_ticket_no")->nullable();
                $table->string("forigen_id_no")->nullable();
                $table->string("lived_date")->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
