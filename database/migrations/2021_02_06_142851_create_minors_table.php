<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMinorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('minors')){
            Schema::create('minors', function (Blueprint $table) {
                $table->id();

                $table->string("fname_nep");
                $table->string("mname_nep")->nullable();
                $table->string("lname_nep");

                $table->string("fname_eng");
                $table->string("mname_eng")->nullable();
                $table->string("lname_eng");

                $table->string("sex_eng");
                $table->string("sex_nep");

                $table->string("birth_place_eng");
                $table->string("birth_place_nep");

                $table->bigInteger("temp_district_id");
                $table->bigInteger("temp_local_level_id");

                $table->bigInteger("per_district_id");
                $table->bigInteger("per_local_level_id");

                $table->string("woda_nep")->nullable();
                $table->string("woda_eng")->nullable();

                $table->string("dob_eng")->nullable();
                $table->string("dob_nep")->nullable();
                
                $table->string("father_name_eng");
                $table->string("father_name_nep");

                $table->string("father_addr_eng");
                $table->string("father_addr_nep");

                $table->string("mother_name_eng")->nullable();
                $table->string("mother_name_nep")->nullable();

                $table->string("mother_addr_eng")->nullable();
                $table->string("mother_addr_nep")->nullable();

                $table->string("mother_citizenship_eng")->nullable();
                $table->string("mother_acitizenship_nep")->nullable();

                $table->string("father_citizenship_eng")->nullable();
                $table->string("father_acitizenship_nep")->nullable();
                

                $table->string("protect_name_nep")->nullable();
                $table->string("protect_name_eng")->nullable();

                $table->string("protect_addr_eng")->nullable();
                $table->string("protect_addr_nep")->nullable();

                $table->string("protect_citizenship_eng")->nullable();
                $table->string("protect_acitizenship_nep")->nullable();

                $table->enum('status', array('pending','verified','rejected'))->default("pending");

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minors');
    }
}
