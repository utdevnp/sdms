<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewcitizenshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newcitizenships', function (Blueprint $table) {
            $table->id();
            $table->string("fname_eng",254);
            $table->string("fname_nep",254);
            $table->string("mname_eng",254);
            $table->string("mname_nep",254);
            $table->string("lname_eng",254);
            $table->string("lname_nep",254);
            $table->string("father_f_name");
            $table->string("father_m_name");
            $table->string("father_l_name");
            $table->string("dobin_eng");
            $table->string("dobin_nep");
            $table->integer("user_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newcitizenships');
    }
}
