<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCitizenshipTableAddBihba extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn("newcitizenships","marriage_date")){
            Schema::table('newcitizenships', function (Blueprint $table) {
                $table->string("marriage_date")->nullable();
                $table->string("marriage_issue_date")->nullable();
                $table->string("father_citi_issue_district")->nullable();
                $table->string("mother_citi_issue_district")->nullable();
                $table->string("proof_citi_issue_district")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
