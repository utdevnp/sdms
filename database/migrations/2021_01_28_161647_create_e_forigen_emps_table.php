<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEForigenEmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable("e_forigen_emps")){
            Schema::create('e_forigen_emps', function (Blueprint $table) {
                $table->id();
                $table->string("name");
                $table->bigInteger("district_id");
                $table->bigInteger("local_level_id");
                $table->string("citizenship_id");
                $table->string("woda");
                $table->string("street");
                $table->string("relation")->nullable();

                $table->string("forigen_emp_name")->nullable();
                $table->string("forigen_emp_country")->nullable();
                $table->string("passport_no")->nullable();
                $table->string("work_for")->nullable();
                $table->string("main_power_name")->nullable();
                $table->string("pass_away_reason")->nullable();
                $table->string("pass_away_date")->nullable();

                $table->string("contact_number")->nullable();
                $table->bigInteger("user_id");
                $table->bigInteger("branch_id");
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_forigen_emps');
    }
}
