<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn("e_forigen_emps","status")){
            Schema::table('e_forigen_emps', function (Blueprint $table) {
                $table->enum('status', array('pending','verified','rejected'))->default("pending");
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('e_forigen_emps', function (Blueprint $table) {
            //
        });
    }
}
