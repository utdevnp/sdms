<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherNagariktaFieldsNewcitizenshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {

            $table->string("gender")->nullable();

            $table->string("dob_district")->nullable();
            $table->string("dob_municpality")->nullable();
            $table->string("dob_wardno")->nullable();

            $table->bigInteger("addr_district")->nullable();
            $table->bigInteger("addr_municipility")->nullable();
            $table->bigInteger("addr_wodano")->nullable();

            $table->string("father_address")->nullable();
            $table->string("father_citizenship_id")->nullable();

            $table->string("mother_name")->nullable();
            $table->string("mother_address")->nullable();
            $table->string("mother_citizenship_id")->nullable();

            $table->string("husband_or_wife_name")->nullable();
            $table->string("husband_or_wife_address")->nullable();
            $table->string("husband_or_wife_citizeship")->nullable();

            $table->string("protector_name")->nullable();
            $table->string("protector_address")->nullable();
            $table->string("protector_citizeship")->nullable();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
