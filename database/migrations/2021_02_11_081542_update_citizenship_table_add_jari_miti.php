<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCitizenshipTableAddJariMiti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn("newcitizenships","father_citi_issue")){
            Schema::table('newcitizenships', function (Blueprint $table) {
                $table->string("father_citi_issue")->nullable();
                $table->string("mother_citi_issue")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newcitizenships', function (Blueprint $table) {
            //
        });
    }
}
