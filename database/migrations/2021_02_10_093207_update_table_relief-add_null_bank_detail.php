<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableReliefAddNullBankDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hascolumn("reliefs","bank_acc")){
            Schema::table('reliefs', function (Blueprint $table) {
                $table->string("bank_acc")->nullable()->change();
                $table->string("bank_name")->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        
    }
}
