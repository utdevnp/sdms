<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrimitiveEthnicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable("primitive_ethnics")){
            Schema::create('primitive_ethnics', function (Blueprint $table) {
                $table->id();
                $table->string("name");
                $table->bigInteger("district_id");
                $table->bigInteger("local_level_id");
                $table->string("citizenship_id")->nullable();
                $table->string("woda")->nullable();
                $table->string("street")->nullable();
                $table->string("father_name")->nullable();
                $table->string("mother_name")->nullable();
                $table->string("cast")->nullable();
                $table->string("contact_number")->nullable();
                $table->bigInteger("user_id");
                $table->bigInteger("branch_id");
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primitive_ethnics');
    }
}
