<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitizenshipChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('citizenship_changes')) {
            Schema::create('citizenship_changes', function (Blueprint $table) {
                $table->id();
                $table->string("curent_citizenship_id");
                $table->string("current_issue_date");
                $table->string("full_name_eng")->nullable();
                $table->string("full_name_nep")->nullable();
                $table->string("date_of_birth")->nullable();
                $table->string("issue_date")->nullable();
                $table->string("father_mother_name")->nullable();
                $table->longText("birth_place")->nullable();

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizenship_changes');
    }
}
