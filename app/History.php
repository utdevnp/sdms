<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class History extends Model
{
    protected $fillable = [
        "status","description","user_id","branch_id",
        "type","category_id"
    ];



    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }
}
