<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class PrimitiveEthnic extends Model
{
    protected $fillable = [
        "name","district_id","local_level_id","citizenship_id",
        'woda',"street","father_name","mother_name","cast","contact_number",
        "user_id","branch_id"
    ];




    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
}
