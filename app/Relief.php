<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Relief extends Model
{
    
    protected $fillable = [
        "full_name","accident_case","accident_date","district_id",
        "locallevel_id","ward_no","street_address","mobile_number",
        "bank_acc","bank_name","user_id","branch_id"
    ];


    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }

}
