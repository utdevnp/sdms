<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
class Minor extends Model
{
    protected $fillable = [
        "fname_nep","mname_nep","lname_nep","fname_eng","mname_eng","lname_eng",
        "sex_eng","sex_nep","birth_place_eng","birth_place_nep",
        "temp_district_id","temp_local_level_id","per_district_id","per_local_level_id",
        "woda_eng","woda_nep",
        "dob_eng","dob_nep",
        "father_name_eng","father_name_nep","father_addr_eng","father_addr_nep","father_citizenship_eng","father_acitizenship_nep",
        "mother_name_eng","mother_name_nep","mother_addr_eng","mother_addr_nep","mother_citizenship_eng","mother_acitizenship_nep",
        "protect_name_nep","protect_name_eng","protect_addr_eng","protect_addr_nep","protect_citizenship_eng","protect_acitizenship_nep",
        "status",
        "user_id","branch_id"

    ];

    protected $appends = ['perdistrict','permunicipility','tempdistrict','tempmunicipility'];

    public function getPerdistrictAttribute(){
        return  DB::table('districts')
        ->where('id', $this->attributes['per_district_id'])
        ->get();
    }

    public function getTempdistrictAttribute(){
        return  DB::table('districts')
        ->where('id', $this->attributes['temp_district_id'])
        ->get();
    }


    public function getPermunicipilityAttribute(){
        return  DB::table('local_levels')
        ->where('id', $this->attributes['per_local_level_id'])
        ->get();
    }

    public function getTempmunicipilityAttribute(){
        return  DB::table('local_levels')
        ->where('id', $this->attributes['temp_local_level_id'])
        ->get();
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
    
}
