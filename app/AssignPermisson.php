<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignPermisson extends Model
{
    protected $fillable = [
        "permission",
        "user_id",
        "user_type"
    ];

    protected $table="assign_permissons";
}
