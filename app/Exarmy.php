<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Exarmy extends Model
{
    protected $fillable = [
        "name","district_id","local_level_id","citizenship_id","woda",
        "street","relation","patta_id","ex_army_name","pass_away_date",
        "famaily_mamber","contact_number","user_id","branch_id","status"
    ];




    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
}
