<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        "name",
        "email_address",
        "contact_number",
        "district_id",
        "municipility_id",
        "woda_id",
        "street_address",
        "postal_code",
        "user_id",
        "officer_name",
        "officer_phone",
        "position"
    ];



    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
}
