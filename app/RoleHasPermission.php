<?php

namespace App;
use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\Model;

class RoleHasPermission extends Model
{
    protected $table="role_has_permissions";

    protected $appends = ['permission_name','role_name',"child_permission"];



    public function getPermissionNameAttribute()
    {
        return Permission::where("id",$this->attributes['permission_id'])->pluck('name')->toArray();
    }

    public function getRoleNameAttribute()
    {
        return Role::where("id",$this->attributes['role_id'])->pluck('name')->toArray();
    }

    public function getChildPermissionAttribute()
    {
        return Permission::where("parent_id",$this->attributes['permission_id'])->pluck('display_name')->toArray();
    }


}
