<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Branch;
class ServiceCenter extends Model
{
    protected $fillable = [
        "name",
        "email_address",
        "contact_number",
        "district_id",
        "municipility_id",
        "woda_id",
        "street_address",
        "postal_code",
        "user_id",
        "officer_name",
        "officer_phone",
        "position",
        "branch_id"
       
    ];

    protected $appends = ["municipility",'ilaka'];

    public function getMunicipilityAttribute($value){
        $municipility =  DB::table('local_levels')->find($this->attributes['municipility_id']);
        return $municipility->name;
       
    }

    public function getIlakaAttribute($value){
        $branch =  Branch::find($this->attributes['branch_id']);
        return $branch->name;
    }


    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
}
