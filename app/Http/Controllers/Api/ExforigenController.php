<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ExforigenRepository;
class ExforigenController extends Controller
{
    function __construct(ExforigenRepository $exforigen, BaseController $response){
        $this->exforigen = $exforigen;
        $this->response = $response;
    }



    public function index()
    {
        $exforigen = $this->exforigen->getAll();

        if(!$exforigen){
            return $this->response->sendError("Ex forigener cannot list","",400);
        }

        return $this->response->sendSuccess($exforigen,"Ex forigener listed successfully");
    }


    public function store()
    {
        $exforigen = $this->exforigen->createUpdate();

        if(!$exforigen){
            return $this->response->sendError("Ex forigner cannot add","",400);
        }

        return $this->response->sendSuccess($exforigen,"Ex forigner added successfully");
    }

    public function show($id)
    {
        $exforigen = $this->exforigen->getById($id);

        if(!$exforigen){
            return $this->response->sendError("Ex forigner cannot list","",400);
        }

        return $this->response->sendSuccess($exforigen,"Ex forigner listed successfully");
    }


    public function update($id)
    {
        $exforigen = $this->exforigen->createUpdate($id);

        if(!$exforigen){
            return $this->response->sendError("Ex forigner cannot update","",400);
        }

        return $this->response->sendSuccess($exforigen,"Ex forigner update successfully");
    }

    public function destroy($id)
    {
        $exforigen = $this->exforigen->delete($id);

        if(!$exforigen){
            return $this->response->sendError("Ex forigner cannot delete","",400);
        }

        return $this->response->sendSuccess($exforigen,"Ex forigner delete successfully");
    }

}
