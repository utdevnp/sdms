<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\ReliefRepository;
use App\Http\Controllers\API\BaseController;

class Reliefcontroller extends Controller
{
    function __construct(ReliefRepository $relief, BaseController $response){
        $this->relief = $relief;
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relief = $this->relief->getAll();

        if(!$relief){
            return $this->response->sendError("Relief cannot list","",400);
        }

        return $this->response->sendSuccess($relief,"Relief listed successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $relief = $this->relief->createUpdate();

        if(!$relief){
            return $this->response->sendError("Relief cannot add","",400);
        }

        return $this->response->sendSuccess($relief,"Relief added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relief = $this->relief->getById($id);

        if(!$relief){
            return $this->response->sendError("Relief cannot list","",400);
        }

        return $this->response->sendSuccess($relief,"Relief listed successfully");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $relief = $this->relief->createUpdate($id);

        if(!$relief){
            return $this->response->sendError("Relief cannot update","",400);
        }

        return $this->response->sendSuccess($relief,"Relief update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $relief = $this->relief->delete($id);

        if(!$relief){
            return $this->response->sendError("Relief cannot delete","",400);
        }

        return $this->response->sendSuccess($relief,"Relief delete successfully");
    }
}
