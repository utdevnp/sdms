<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ServicecenterRepository;
use App\Http\Controllers\API\BaseController;

class ServicecenterController extends Controller
{
    public function __construct(
        ServicecenterRepository $servicecenter,
        BaseController $response
    ){
        $this->servicecenter = $servicecenter;
        $this->response = $response;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
        $servicecenter =  $this->servicecenter->getAll();
        if($servicecenter){
            return $this->response->sendSuccess($servicecenter, "Service center listed successfully");
        }else{
            return $this->response->sendError("Service center cannot be listed ");
        }
    }catch(Throwable $e){
        report($e);
        return false;
    }
       
    }

  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try{
            $servicecenter = $this->servicecenter->createUpdate();
            if($servicecenter){
                return $this->response->sendSuccess($servicecenter,"service center created successfully.");
            }else{
                return $this->response->sendError($servicecenter,"service center cannot created .");
            }
        }catch(Throwable $e){
            report($e);
            return false;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $servicecenter  = $this->servicecenter->getById($id);
        return $this->response->sendSuccess($servicecenter,"Service center listed successfully.");
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        try{
            $servicecenter = $this->servicecenter->createUpdate($id);
            if($servicecenter){
                return $this->response->sendSuccess($servicecenter,"service center update successfully.");
            }else{
                return $this->response->sendError($servicecenter,"service center cannot update .");
            }
        }catch(Throwable $e){
            report($e);
            return false;
        }
    }

    public function getServiceById($id){
        try{
            $servicecenter = $this->servicecenter->getByUserId($id);
            if($servicecenter){
                return $this->response->sendSuccess($servicecenter,"service center listed successfully.");
            }else{
                return $this->response->sendError($servicecenter,"service center cannot list .");
            }
        }catch(Throwable $e){
            report($e);
            return false;
        }

    }


    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicecenter = $this->servicecenter->delete($id);
        return $this->response->sendSuccess($servicecenter,"Service center delete successfully");
    }
}
