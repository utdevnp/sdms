<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\DistrictRepository;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function __construct(
        DistrictRepository $district,
        BaseController $response
    ){
        $this->district = $district;
        $this->response = $response;
    }

    public function index(){
        $district = $this->district->getAll();

        if(!$district){
            return $this->response->sendError("District cannot list","",400);
        }

        return $this->response->sendSuccess($district, "Districts listed successfully");
    }

    public function show($id){
        $district = $this->district->getById($id);

        if(!$district){
            return $this->response->sendError("District cannot get","",400);
        }

        return $this->response->sendSuccess($district, "Districts getting successfully");
    }
}
