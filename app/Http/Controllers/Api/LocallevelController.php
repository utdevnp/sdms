<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\LocallevelRepository;
use Illuminate\Http\Request;

class LocallevelController extends Controller
{
    public function __construct(
        LocallevelRepository $locallevel,
        BaseController $response
    ){
        $this->locallevel = $locallevel;
        $this->response = $response;
    }

    public function index(){
        $locallevel = $this->locallevel->getAll();

        if(!$locallevel){
            return $this->response->sendError("Cannot list local level","",400);
        }

        return $this->response->sendSuccess($locallevel,"Local level listed successfully");
    }

    public function getByDistrictId($id){
        $locallevel = $this->locallevel->getByDistrictId($id);

        if(!$locallevel){
            return $this->response->sendError("Cannot list  local level district","",400);
        }

        return $this->response->sendSuccess($locallevel,"Local level district listed successfully");
    }

    public function show($id){
        $locallevel = $this->locallevel->getById($id);

        if(!$locallevel){
            return $this->response->sendError("Cannot get local level","",400);
        }

        return $this->response->sendSuccess($locallevel,"Local level getting successfully");
    }
}
