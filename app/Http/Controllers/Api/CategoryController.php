<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Http\Controllers\API\BaseController;
class CategoryController extends Controller
{
    private $category;
    private $response;

    public function __construct(
        CategoryRepository $category,
        BaseController $response
    ){
        $this->category = $category;
        $this->response = $response;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = $this->category->getAll();

        if(!$category){
            return $this->response->sendError("Category Cannot list","",404);
        }
        return $this->response->sendSuccess($category,"category Listed success.");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $category = $this->category->createUpdate();

        if(!$category){
            return $this->response->sendError("Category cannot create","","400");
        }

        return $this->response->sendSuccess($category,"Category created successsfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category  = $this->category->getById($id);

        if(!$category){
            return $this->response->sendError("Category cannot get","",400);
        }

        return $this->response->sendSuccess($category,"Category listed successfully .");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $category = $this->category->createUpdate($id);

        if(!$category){
            return $this->response->sendError("Category cannot update","","400");
        }

        return $this->response->sendSuccess($category,"Category update successfully.");
    }


    public function categoryWithParent(){
        try{
            $category = $this->category->categoryWithParent();
            if(! $category){
                return $this->response->sendError("Category cannot get", "", 400);  
            }
            
            return $this->response->sendSuccess($category,"Category with parent listed successfully");
            
        }catch(Throwable $e){
            report($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category  = $this->category->delete($id);

        if(!$category){
            return $this->response->sendError("Category cannot delete","","400");
        }
        return $this->response->sendSuccess($category,"Category deleted successfully.");
    }
}
