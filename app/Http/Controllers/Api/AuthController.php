<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Http\Controllers\API\BaseController;
use Validator;
use Illuminate\Http\Request;
class AuthController extends Controller
{

    private $userRepository;
    private $response;
    private $request;

    public function __construct(
        UserRepository $userRepository,
        BaseController $response,
        Request $request
    ){
        $this->userRepository = $userRepository;
        $this->response  = $response;
        $this->request = $request;
    }

    public function index(){
        $user = $this->userRepository->getAll();

        if(! $user){
            return $this->response->sendError("Cannot list user", $user, 400);
        }
        return $this->response->sendSuccess($user, 'User list successfull');
    }

    public function getuser()
    {
        $user = $this->userRepository->user();
        return $this->response->sendSuccess($user, 'user listed success.');
    }

    public function login()
    {
       
        $validator = Validator::make($this->request->all(), [ 
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        if($validator->fails()) { 
            return  $this->response->sendError('Validation Error.',['error'=>$validator->errors()],401);            
        }

        $user = $this->userRepository->login();

        if($user == false){
            return $this->response->sendError("Unauthorized", $user, 401);
        }
        return $this->response->sendSuccess($user, 'Login successful');
    }

    public function signup(){

        $validator = Validator::make($this->request->all(), [ 
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:8'
        ]);

        if($validator->fails()) { 
            return $this->response->sendError('Validation Error',['error'=>$validator->errors()],400);            
        }else{
            $user = $this->userRepository->register();

            if($user == false){
    
                return $this->response->sendError("Register unsuccessful", $user, 400);
            }
    
            return $this->response->sendSuccess($user, 'Register successful');
        }

       
    }

    public function logout(){
        $this->userRepository->logout();
        return $this->response->sendSuccess("", 'Logout successful');
    }

    public function addRole(){
        $user = $this->userRepository->createRole();

        if(! $user){
    
            return $this->response->sendError("Role cannot create", $user, 400);
        }

        return $this->response->sendSuccess($user, 'Role Crated successfully');
    }

    public function addPermission(){

        $user = $this->userRepository->createPermission();

        if(! $user){
    
            return $this->response->sendError("Permission cannot create", $user, 400);
        }

        return $this->response->sendSuccess($user, 'Permission Crated successfully');
    }

    public function getRoles(){
        $user = $this->userRepository->getRoles();

        if(! $user){
    
            return $this->response->sendError("Roles cannot be listed", $user, 400);
        }

        return $this->response->sendSuccess($user, 'Roles listed successfully');
    }

    public function assignRole(){
        $user = $this->userRepository->assignRole();

        if(!$user){
            return $this->response->sendError("Roles cannot be assigned", $user, 400);
        }

        return $this->response->sendSuccess($user, 'Roles assigned successfully');
    }


    public function assignedRoles(){
        $user = $this->userRepository->assignedRoles();
        if(!$user){
            return $this->response->sendError("Roles cannot be assigned", $user, 400);
        }

        return $this->response->sendSuccess($user, 'Roles assigned successfully');
    }

    public function show($id){
        $user = $this->userRepository->getById($id);

        if(!$user){
            return $this->response->sendError("User cannot to be get", "", 400);
        }

        return $this->response->sendSuccess($user, 'User listed successfully');
    }


    public function changePass($id){
        $validator = Validator::make($this->request->all(), [ 
            'password' => 'required|string|min:8',
            'c_password' => 'required|string|min:8|same:password'
        ]);

        if($validator->fails()) { 
            return $this->response->sendError('Validation Error',['error'=>$validator->errors()],411);            
        }else{
            $user = $this->userRepository->changePass($id);

            if(!$user){
                return $this->response->sendError("Password can't change", "", 400);
            }
    
            return $this->response->sendSuccess($user, 'Password change successfully');
        }
    }

  
    
}
