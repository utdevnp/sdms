<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\MinorRepository;
use App\Http\Controllers\API\BaseController;
class MinorController extends Controller
{

    function __construct(MinorRepository $minor, BaseController $response){
        $this->minor = $minor;
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $minor = $this->minor->getAll();

        if(!$minor){
            return $this->response->sendError("Minor cannot list","",400);
        }

        return $this->response->sendSuccess($minor,"Minor listed successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $minor = $this->minor->createUpdate();

        if(!$minor){
            return $this->response->sendError("Minor cannot add","",400);
        }

        return $this->response->sendSuccess($minor,"Minor added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $minor = $this->minor->getById($id);

        if(!$minor){
            return $this->response->sendError("minor cannot list","",400);
        }

        return $this->response->sendSuccess($minor,"minor listed successfully");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $minor = $this->minor->createUpdate($id);

        if(!$minor){
            return $this->response->sendError("minor cannot update","",400);
        }

        return $this->response->sendSuccess($minor,"minor update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $minor = $this->proof->delete($id);

        if(!$minor){
            return $this->response->sendError("minor cannot delete","",400);
        }

        return $this->response->sendSuccess($minor,"minor delete successfully");
    }
}
