<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CitizenshipChangeRepository;
use App\Http\Controllers\API\BaseController;
class CitizenshipChangeController extends Controller
{

    private $citizenchange;

    public function __construct(
        CitizenshipChangeRepository $citizenchange,
        BaseController $response
    ){
        $this->citizenchange = $citizenchange;
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citichange = $this->citizenchange->getAll();

        if(!$citichange){
            return $this->response->sendError("citizenship change cannot list","",400);
        }

        return $this->response->sendSuccess($citichange,"Citizenship change listed successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $citichange = $this->citizenchange->createUpdate();

        if(!$citichange){
            return $this->response->sendError("citizenship change add fail","",400);
        }

        return $this->response->sendSuccess($citichange,"Citizenship change added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $citichange = $this->citizenchange->getById($id);

        if(!$citichange){
            return $this->response->sendError("citizenship change listed fail","",400);
        }

        return $this->response->sendSuccess($citichange,"Citizenship change listed successfully");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $citichange = $this->citizenchange->createUpdate($id);

        if(!$citichange){
            return $this->response->sendError("citizenship change update fail","",400);
        }

        return $this->response->sendSuccess($citichange,"Citizenship change update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $citichange = $this->citizenchange->delete($id);

        if(!$citichange){
            return $this->response->sendError("citizenship change delete fail","",400);
        }

        return $this->response->sendSuccess($citichange,"Citizenship change delete successfully");
    }
}
