<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BranchRepository;
use App\Http\Controllers\API\BaseController;
class BranchController extends Controller
{
    public function __construct(
        BranchRepository $branch,
        BaseController $response,
        Request $request
    ){
        $this->branch = $branch;
        $this->response = $response;
        $this->request  = $request;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $branch = $this->branch->getAll();
            if($branch){
                return $this->response->sendSuccess($branch,"Branchs Listed");
            }else{
                return $this->response->sendError("Error","Cannot list branch",404);
            }

        } catch (Throwable $e) {

            report($e);
            return false;
        }
        
        
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {  
        $branch  = $this->branch->createUpdate();
        return $this->response->sendSuccess($branch,"Branch created successfully ");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branch = $this->branch->findById($id);
        return $this->response->sendSuccess($branch, "Branch listed success");
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $branch = $this->branch->createUpdate($id);
        return $this->response->sendSuccess($branch, "Branch update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch  = $this->branch->delete($id);
        return $this->response->sendSuccess($branch,"Branch delete successfully");
    }
}
