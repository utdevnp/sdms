<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FileuploadRepository;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class FileuploadController extends Controller
{
    private $fileupload;
    private $response;

    public function __construct(
        FileuploadRepository $fileupload,
        BaseController $response
    ){
        $this->fileupload = $fileupload;
        $this->response = $response;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $fileupload = $this->fileupload->uploadFiles();

        if( !$fileupload){
            return $this->response->sendError("File upload fail","",400);
        }
        return $this->response->sendSuccess($fileupload,"File upload success.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = $this->fileupload->getById($id);
        print_r($image["file"]);
     

       $image  = storage_path("public/".$image["file"]);


       // echo "<img src='".$image."'/>";

        return $this->response->sendSuccess( $image,"File upload success.");

      
    }

    public function getByCatIdWithType(){
        $fileupload = $this->fileupload->getByCatIdWithType();
        if(!$fileupload){
            return $this->response->sendError("File with category listing fail","",400);
        }
        return $this->response->sendSuccess($fileupload,"File with category listed category");
    }


    public function getByCatId($id){
        $fileupload = $this->fileupload->getByCatId($id);

        if(!$fileupload){
            return $this->response->sendError("File with category listing fail","",400);
        }
        return $this->response->sendSuccess($fileupload,"File with category listed category");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fileupload = $this->fileupload->delete($id);

        if(!$fileupload){
            
            return $this->response->sendError("File cannot be delete.","",404);
        }
        return $this->response->sendSuccess($fileupload,"File delete succesfully.");
    }
}
