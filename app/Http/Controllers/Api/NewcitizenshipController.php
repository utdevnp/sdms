<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\NewcitizenshipRepository;
use Illuminate\Http\Request;

class NewcitizenshipController extends Controller
{
    public function __construct(
        NewcitizenshipRepository $newcitizenship,
        BaseController $response
    ){
        $this->newcitizenship  = $newcitizenship;
        $this->response = $response;
    }

    public function index()
    {
        $newcitizenship = $this->newcitizenship->getAll();

        if(!$newcitizenship){
            return $this->response->sendError("Citizenship list cannot get","",400);
        }

        return $this->response->sendSuccess($newcitizenship,"Citizenship listed successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $newcitizenship = $this->newcitizenship->createUpdate();

        if(!$newcitizenship){
            return $this->response->sendError("Citizenship cannot create ","",400);
        }

        return $this->response->sendSuccess($newcitizenship,"Citizenship created successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $newcitizenship = $this->newcitizenship->getById($id);

        if(!$newcitizenship){
            return $this->response->sendError("Citizenship  cannot get","",400);
        }

        return $this->response->sendSuccess($newcitizenship,"Citizenship listed successfully");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $newcitizenship = $this->newcitizenship->createUpdate($id);

        if(!$newcitizenship){
            return $this->response->sendError("Citizenship cannot update ","",400);
        }

        return $this->response->sendSuccess($newcitizenship,"Citizenship updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newcitizenship = $this->newcitizenship->delete($id);

        if(!$newcitizenship){
            return $this->response->sendError("Citizenship  cannot delete","",400);
        }

        return $this->response->sendSuccess($newcitizenship,"Citizenship deleted successfully");
    }


    public function getByLimit($limit){
        $newcitizenship = $this->newcitizenship->getcitizenByLimit($limit);

        if(!$newcitizenship){
            return $this->response->sendError("Citizenship  cannot get","",400);
        }

        return $this->response->sendSuccess($newcitizenship,"Citizenship getting successfully");
    }
}
