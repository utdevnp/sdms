<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\DashboardRepository;
use App\Http\Controllers\API\BaseController;
class DashboardController extends Controller
{
    function __construct(DashboardRepository $dashboard,BaseController $response){
        $this->dashboard = $dashboard;
        $this->response = $response;
    }

    function index(){
        $dashboard = $this->dashboard->getAll();

        if(!$dashboard){
            return $this->response->sendError("Dashbaord data cannot list","",400);
        }

        return $this->response->sendSuccess($dashboard,"Dashboard data listed successfully");
    }

    public function pending($status){
        
        $dashboard = $this->dashboard->getAllPending($status);
        if(!$dashboard){
            return $this->response->sendError("Dashbaord pending data cannot list","",400);
        }

        return $this->response->sendSuccess($dashboard,"Dashboard pending data listed successfully");
    }

    public function report($status){
        $dashboard = $this->dashboard->getAllReport($status);
        if(!$dashboard){
            return $this->response->sendError("Dashbaord report data cannot list","",400);
        }

        return $this->response->sendSuccess($dashboard,"Dashboard report data listed successfully");
    }
}
