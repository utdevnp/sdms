<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\AssignPermissionRepository;
use Illuminate\Http\Request;

class AssignPermissionController extends Controller
{
    public function __construct(
        AssignPermissionRepository $permission,
        BaseController $response
    ){
        $this->permission = $permission;
        $this->response = $response;
    }

    public function index(){
        $permission  = $this->permission->assign();

        if(! $permission){
            return $this->response->sendError("Unable to assign permission","",400);
        }

        return $this->response->sendSuccess($permission,"Assigned successfully");
    }
}
