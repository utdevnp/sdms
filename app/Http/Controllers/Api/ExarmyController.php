<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\ExarmyRepository;

class ExarmyController extends Controller
{
    function __construct(ExarmyRepository $exarmy, BaseController $response){
        $this->exarmy = $exarmy;
        $this->response = $response;
    }



    public function index()
    {
        $exarmy = $this->exarmy->getAll();

        if(!$exarmy){
            return $this->response->sendError("Exarmy cannot list","",400);
        }

        return $this->response->sendSuccess($exarmy,"Exarmy listed successfully");
    }


    public function store()
    {
        $exarmy = $this->exarmy->createUpdate();

        if(!$exarmy){
            return $this->response->sendError("Exarmy cannot add","",400);
        }

        return $this->response->sendSuccess($exarmy,"Exarmy added successfully");
    }

    public function show($id)
    {
        $exarmy = $this->exarmy->getById($id);

        if(!$exarmy){
            return $this->response->sendError("Exarmy cannot list","",400);
        }

        return $this->response->sendSuccess($exarmy,"Exarmy listed successfully");
    }


    public function update($id)
    {
        $exarmy = $this->exarmy->createUpdate($id);

        if(!$exarmy){
            return $this->response->sendError("Exarmy cannot update","",400);
        }

        return $this->response->sendSuccess($exarmy,"Exarmy update successfully");
    }

    public function destroy($id)
    {
        $exarmy = $this->exarmy->delete($id);

        if(!$exarmy){
            return $this->response->sendError("Exarmy cannot delete","",400);
        }

        return $this->response->sendSuccess($exarmy,"Exarmy delete successfully");
    }

}
