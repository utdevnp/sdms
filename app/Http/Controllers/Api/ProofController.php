<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\ProofRepository;

class ProofController extends Controller
{
    function __construct(ProofRepository $proof, BaseController $response){
        $this->proof = $proof;
        $this->response = $response;
    }


    public function index()
    {
        $proof = $this->proof->getAll();

        if(!$proof){
            return $this->response->sendError("Proof cannot list","",400);
        }

        return $this->response->sendSuccess($proof,"Proof listed successfully");
    }


    public function store()
    {
        $proof = $this->proof->createUpdate();

        if(!$proof){
            return $this->response->sendError("Proof cannot add","",400);
        }

        return $this->response->sendSuccess($proof,"Proof added successfully");
    }


    public function show($id)
    {
        $proof = $this->proof->getById($id);

        if(!$proof){
            return $this->response->sendError("Proof cannot list","",400);
        }

        return $this->response->sendSuccess($proof,"Proof listed successfully");
    }


    public function update($id)
    {
        $proof = $this->proof->createUpdate($id);

        if(!$proof){
            return $this->response->sendError("Proof cannot update","",400);
        }

        return $this->response->sendSuccess($proof,"Proof update successfully");
    }

    public function destroy($id)
    {
        $proof = $this->proof->delete($id);

        if(!$proof){
            return $this->response->sendError("Proof cannot delete","",400);
        }

        return $this->response->sendSuccess($proof,"Proof delete successfully");
    }

    
}
