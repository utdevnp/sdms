<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\PrimitiveEthnicRepository;

class PrimitiveEthnicController extends Controller
{
    function __construct(PrimitiveEthnicRepository $etchnic, BaseController $response){
        $this->etchnic = $etchnic;
        $this->response = $response;
    }


    public function index()
    {
        $etchnic = $this->etchnic->getAll();

        if(!$etchnic){
            return $this->response->sendError("Etchnic cannot list","",400);
        }

        return $this->response->sendSuccess($etchnic,"Etchnic listed successfully");
    }


    public function store()
    {
        $etchnic = $this->etchnic->createUpdate();

        if(!$etchnic){
            return $this->response->sendError("Etchnic cannot add","",400);
        }

        return $this->response->sendSuccess($etchnic,"Etchnic added successfully");
    }

    public function show($id)
    {
        $etchnic = $this->etchnic->getById($id);

        if(!$etchnic){
            return $this->response->sendError("Etchnic cannot list","",400);
        }

        return $this->response->sendSuccess($etchnic,"Etchnic listed successfully");
    }


    public function update($id)
    {
        $etchnic = $this->etchnic->createUpdate($id);

        if(!$etchnic){
            return $this->response->sendError("Etchnic cannot update","",400);
        }

        return $this->response->sendSuccess($etchnic,"Etchnic update successfully");
    }

    public function destroy($id)
    {
        $etchnic = $this->etchnic->delete($id);

        if(!$etchnic){
            return $this->response->sendError("Etchnic cannot delete","",400);
        }

        return $this->response->sendSuccess($etchnic,"Etchnic delete successfully");
    }
}
