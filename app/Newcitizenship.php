<?php

namespace App;
use App\Locallevel;
use App\District;
use Illuminate\Database\Eloquent\Model;
class Newcitizenship extends Model
{


    protected $fillable = [
        "fname_eng","fname_nep","mname_eng","mname_nep","lname_eng","lname_nep",
        "father_f_name","father_m_name","father_l_name","father_address","father_citizenship_id",
        "dobin_eng","dobin_nep",
        "gender",
        "dob_district","dob_municpality","dob_wardno",
        "addr_district","addr_municipility","addr_wodano",
        "mother_name","mother_address","mother_citizenship_id",
        "husband_or_wife_name","husband_or_wife_address","husband_or_wife_citizeship",
        "protector_name","protector_address","protector_citizeship" ,
        "user_id",
        "birth_place","type","status",
        "branch_id",
        "contact_number",
        "birth_id_no","birth_issue_date","certificate_name","certificate_dob","migration_issue_date",
        "migration_issue_number","postal_ticket_no","forigen_id_no","lived_date",
        "proof_name","proof_citizen_id","proof_relation",
        "father_citi_issue","mother_citi_issue","marriage_date","marriage_issue_date",
        "father_citi_issue_district","mother_citi_issue_district","proof_citi_issue_district"
    ];
    

    protected $appends = ['district_nep','district_eng','local_level_nep',"local_level_eng"];

    public function getDistrictNepAttribute(){
        $district = District::find($this->attributes['addr_district']);
        return $district->name_nep;
    }

    public function getDistrictEngAttribute(){
        $district = District::find($this->attributes['addr_district']);
        return $district->name_eng;
    }
    public function getLocalLevelNepAttribute(){
        $district = Locallevel::find($this->attributes['addr_municipility']);
        return $district->name;
    } 
    public function getLocalLevelEngAttribute(){
        $district = Locallevel::find($this->attributes['addr_municipility']);
        return $district->name_eng;
    }
}
