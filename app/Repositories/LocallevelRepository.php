<?php

namespace App\Repositories;

use App\Locallevel;

 
class LocallevelRepository 
{
    public function __construct(
        Locallevel $locallevel
    ){
        $this->locallevel = $locallevel;
    }

    public function getAll(){
        return $this->locallevel->get();
    }

    public function getByDistrictId($id){
        return $this->locallevel->where("district_id",$id)->get();
    }

    public function getById($id){
        return $this->locallevel->find($id);
    }
}
