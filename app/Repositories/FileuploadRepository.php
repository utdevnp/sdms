<?php

namespace App\Repositories;

use App\Fileupload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
/**
 * Class FileuploadRepository.
 */
class FileuploadRepository{
   
    private $fileupload;
    private $request;
    
    public function __construct(
        Fileupload $fileupload,
        Request $request
    ){
        $this->fileupload = $fileupload;
        $this->request = $request;
    }

    public function getById($id){
        return  $fileupload = $this->fileupload->find($id);
    }

    public function getByCatId($catid){
        return $this->fileupload->where("category_id",$catid)->orderBy('id','DESC')->get();
    }

    public function getByCatIdWithType(){
        $catid = $this->request->catid;
        $type = $this->request->type;
        return $this->fileupload
            ->where("category_id",$catid)
            ->where("type",$type)
            ->orderBy('id','DESC')
            ->get();
    }

    public function uploadFiles(){

        $this->request->validate([
            'file' => 'required|mimes:pdf,jpg,png|max:2048'
            ]);
    
            $fileupload = $this->fileupload;
    
            if($this->request->file()) {
                $fileName = time().'_'.$this->request->file->getClientOriginalName();
                $filePath = $this->request->file('file')->storeAs('uploads', $fileName, 'public');
                
    
                $fileupload->file = time().'_'.$this->request->file->getClientOriginalName();
                $fileupload->file_path =   base64_encode(file_get_contents($this->request->file('file')));
                $fileupload->file_name = $this->request->file_name;
                $fileupload->file_description = $this->request->file_description;
                $fileupload->file_size = $this->request->file("file")->getSize();
                $fileupload->user_id =  Auth::id();
                $fileupload->category_id = $this->request->category_id;
                $fileupload->type = $this->request->type;
                $fileupload->save();
    
                return $fileupload;
            }


    
    }

    public function delete($id){
        $fileupload = $this->fileupload->find($id);
        //return $fileupload;
        if($fileupload){
            Storage::delete('/public/uploads/'.$fileupload->file);
           
            $this->fileupload->destroy($id);
            return $fileupload;
            
        }else{
            return false;
        }
    }

}

