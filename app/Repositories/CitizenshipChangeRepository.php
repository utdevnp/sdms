<?php

namespace App\Repositories;
use App\CitizenshipChange;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Repositories\ServiceCenterRepository;
/**
 * Class CitizenshipChangeRepository.
 */
class CitizenshipChangeRepository 
{
    function __construct(
        CitizenshipChange $citichange,
        BranchRepository $branch,
        AssignPermissionRepository $role,
        UserRepository $user,
        Request $request,
        ServiceCenterRepository $servicecenter
        
    ){
        $this->citichange = $citichange;
        $this->branch = $branch;
        $this->role = $role;
         $this->user = $user;
         $this->request = $request;
         $this->servicecenter = $servicecenter;
    }

    public function getAll(){
        if($this->role->checkRole() == "branch"){
            
            $branch = $this->branch->findBranchIdByUserId();
            return $this->citichange->where("branch_id",$branch)->get();
        }else if($this->role->checkRole()=="user"){

            $userid = $this->user->getCurrentUserId();
            return $this->citichange->where("user_id",$userid)->get();
        }else{
            return $this->citichange->get();
        }
    }

    public function getById($id){
        return $this->citichange->find($id);
    }

    public function createUpdate($id=null){

        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        $user_id = $this->user->getCurrentUserId();

        $citizensip = array(
            "curent_citizenship_id"=>$this->request->curent_citizenship_id,
            "current_issue_date"=>$this->request->current_issue_date,
            "full_name_eng"=>$this->request->full_name_eng,
            "full_name_nep"=>$this->request->full_name_nep,
            "date_of_birth"=>$this->request->date_of_birth,
            "issue_date"=>$this->request->issue_date,
            "father_mother_name"=>$this->request->father_mother_name,
            "birth_place"=>$this->request->birth_place,
            "user_id"=>$user_id,
            "branch_id"=>$branch_id,
            "contact_number"=>$this->request->contact_number
        );

        $getcitizen = $this->citichange->find($id);

        if($getcitizen){
            $getcitizen->update($citizensip);
            return $getcitizen;
        }else{
            return $this->citichange->create($citizensip);
        }
    }


    function updateStatus($id,$status){
        $status = array(
            "status"=>$status
        );
        $newciti = $this->citichange->find($id);
        $newciti->update($status);
    }


    public function delete($id){
        return $this->citichange->destroy($id);
    }
}
