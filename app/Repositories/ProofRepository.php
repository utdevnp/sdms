<?php

namespace App\Repositories;
use App\Proof;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
/**
 * Class ProofRepository.
 */
class ProofRepository
{
   
    private $proof;

    function __construct(
         Proof $proof,
         BranchRepository $branch,
         AssignPermissionRepository $role,
         UserRepository $user,
         Request $request,
         ServiceCenterRepository $servicecenter
    ){
         $this->proof = $proof;
         $this->branch = $branch;
         $this->role = $role;
          $this->user = $user;
          $this->request = $request;
          $this->servicecenter = $servicecenter;
    }

    function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->branch->findBranchIdByUserId();
            return $this->proof->where("branch_id",$branch)->orderBy('id','DESC')->get();
        }else if($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->proof->where("user_id",$userid)->orderBy('id','DESC')->get();
        }else{
            return $this->proof->orderBy('id','DESC')->get();
        }
   }

   function createUpdate($id=null){

        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        $user_id = $this->user->getCurrentUserId();

        $proofdata = array(
            "name"=>$this->request->name,
            "district_id"=>$this->request->district_id,
            "local_level_id"=>$this->request->local_level_id,
            "citizenship_id"=>$this->request->citizenship_id,
            "woda"=>$this->request->woda,
            "father_name"=>$this->request->father_name,
            "mother_name"=>$this->request->mother_name,
            "grand_father_name"=>$this->request->grand_father_name,
            "contact_number"=>$this->request->contact_number,
            "user_id"=>$user_id,
            "branch_id"=>$branch_id,
            "type"=>$this->request->type,
            "husband_wife"=>$this->request->husband_wife
        );

        $getProof = $this->proof->find($id);
        if($getProof){
            $getProof->update($proofdata);
            return $getProof;
        }else{
            return $this->proof->create($proofdata);
        }

   }

   function updateStatus($id,$status){
    $statusa = array(
        "status"=>$status
    );
    $citizench = $this->proof->find($id);
    $citizench->update($statusa);
}




    function getById($id){
        return $this->proof->find($id);
    }

    function delete($id){
        return $this->proof->destroy($id);
    }


}
