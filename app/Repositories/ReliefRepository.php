<?php

namespace App\Repositories;
use App\Relief;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
/**
 * Class ReliefRepository.
 */

class ReliefRepository
{
    private $relief;

   function __construct(
        Relief $relief,
        BranchRepository $branch,
        AssignPermissionRepository $role,
        UserRepository $user,
        Request $request,
        ServiceCenterRepository $servicecenter
   ){
        $this->relief = $relief;
        $this->branch = $branch;
        $this->role = $role;
         $this->user = $user;
         $this->request = $request;
         $this->servicecenter = $servicecenter;
   }

   function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->branch->findBranchIdByUserId();
            return $this->relief->where("branch_id",$branch)->orderBy('id','DESC')->get();
        }else if($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->relief->where("user_id",$userid)->orderBy('id','DESC')->get();
        }else{
            return $this->relief->orderBy('id','DESC')->get();
        }
   }


   function createUpdate($id=null){

        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        $user_id = $this->user->getCurrentUserId();

       $reliefdata = array(
        "full_name"=>$this->request->full_name,
        "accident_case"=>$this->request->accident_case,
        "accident_date"=>$this->request->accident_date,
        "district_id"=>$this->request->district_id,
        "locallevel_id"=>$this->request->locallevel_id,
        "ward_no"=>$this->request->ward_no,
        "street_address"=>$this->request->street_address,
        "mobile_number"=>$this->request->mobile_number,
        "bank_acc"=>$this->request->bank_acc,
        "bank_name"=>$this->request->bank_name,
        "user_id"=>$user_id,
        "branch_id"=>$branch_id
       );

       $getRelief = $this->relief->find($id);
       if($getRelief){
           $getRelief->update($reliefdata);
           return $getRelief;
       }else{
           return $this->relief->create($reliefdata);
       }

   }

   function getById($id){
       return $this->relief->find($id);
   }

   function delete($id){
       return $this->relief->destroy($id);
   }


}
