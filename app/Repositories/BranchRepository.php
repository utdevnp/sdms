<?php

namespace App\Repositories;
use App\AssignPermisson;
use App\Branch;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
/**
 * Class BranchRepository.
 */
class BranchRepository 
{
   public function __construct(
       Branch $branch,
       Request $request,
       UserRepository $user,
       AssignPermisson $permission
   ){
        $this->branch = $branch;
        $this->request = $request;
        $this->user = $user;
        $this->permission = $permission;
   }


   public function getAll(){
        return $this->branch->orderBy('id','DESC')->get();
   }

   public function createUpdate($id=null){

        if($id==null){
            $userCreate = $this->user->registerFother(
                $this->request->name,
                $this->request->email_address,
                ""
            );

            $userid = $userCreate->id;

            $permissonadd = array(
                "user_id"=>$userid,
                "user_type"=>"branch",
                "permission"=>" "
            );

            $this->permission->create($permissonadd);

        }else{
            $userid = $this->request->user_id;
        }


        $branch = array(
            "name" => $this->request->name,
            "email_address" => $this->request->email_address,
            "contact_number" => $this->request->contact_number,
            "district_id" => $this->request->district_id,
            "municipility_id" => $this->request->municipility_id,
            "woda_id" => $this->request->woda_id,
            "street_address" => $this->request->street_address,
            "postal_code" => $this->request->postal_code,
            "user_id" => $userid,
            "officer_name"=>$this->request->officer_name,
            "officer_phone"=>$this->request->officer_phone,
            "position"=>$this->request->position
        );

        $getBranch = $this->branch->find($id);

        if($getBranch){
        $getBranch->update($branch);
        return $getBranch;
        }else{
            return $this->branch->create($branch);
        }

        

   }

   public function findById($id){
    return $this->branch->find($id);
   }

   public function delete($id){
    return $this->branch->destroy($id);
   }

   // return branch id;
   public function findBranchIdByUserId(){
        $id = $this->user->getCurrentUserId();
        $branch =  $this->branch->where("user_id",$id)->get();
        return @$branch[0]['id'];
   }

   


}
