<?php

namespace App\Repositories;
use App\PrimitiveEthnic;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

/**
 * Class PrimitiveEthnicRepository.
 */
class PrimitiveEthnicRepository 
{
    

    private $etchnic;

    function __construct(
         PrimitiveEthnic $etchnic,
         BranchRepository $branch,
         AssignPermissionRepository $role,
         UserRepository $user,
         Request $request,
         ServiceCenterRepository $servicecenter
    ){
        $this->etchnic = $etchnic;
        $this->branch = $branch;
        $this->role = $role;
        $this->user = $user;
        $this->request = $request;
        $this->servicecenter = $servicecenter;
    }

    function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->branch->findBranchIdByUserId();
            return $this->etchnic->where("branch_id",$branch)->get();
        }else if($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->etchnic->where("user_id",$userid)->get();
        }else{
            return $this->etchnic->get();
        } 
   }

    function createUpdate($id=null){

        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        $user_id = $this->user->getCurrentUserId();

        $getetchnicdata = array(
            "name"=>$this->request->name,
            "district_id"=>$this->request->district_id,
            "local_level_id"=>$this->request->local_level_id,
            "citizenship_id"=>$this->request->citizenship_id,
            'woda'=>$this->request->woda,
            "street"=>$this->request->street,
            "father_name"=>$this->request->father_name,
            "mother_name"=>$this->request->mother_name,
            "cast"=>$this->request->cast,
            "contact_number"=>$this->request->contact_number,
            "user_id"=>$user_id,
            "branch_id"=>$branch_id
        );

        $getetchnic = $this->etchnic->find($id);
        if($getetchnic){
            $getetchnic->update($getetchnicdata);
            return $getetchnic;
        }else{
            return $this->etchnic->create($getetchnicdata);
        }

    }


    function getById($id){
        return $this->etchnic->find($id);
    }

    function delete($id){
        return $this->etchnic->destroy($id);
    }

   

}
