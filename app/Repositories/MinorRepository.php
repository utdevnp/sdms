<?php

namespace App\Repositories;

use App\Minor;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
/**
 * Class MinorRepository.
 */
class MinorRepository
{
 
    public function __construct(
        Minor $minor,
        BranchRepository $branch,
        AssignPermissionRepository $role,
        UserRepository $user,
        Request $request,
        ServiceCenterRepository $servicecenter
    ){
        $this->minor = $minor;
        $this->branch = $branch;
        $this->role = $role;
        $this->user = $user;
        $this->request = $request;
        $this->servicecenter = $servicecenter;
    }

    function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->branch->findBranchIdByUserId();
            return $this->minor->where("branch_id",$branch)->orderBy('id','DESC')->get();
        }else if($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->minor->where("user_id",$userid)->orderBy('id','DESC')->get();
        }else{
            return $this->minor->orderBy('id','DESC')->get();
        }
   }

    function createUpdate($id=null){

        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        $user_id = $this->user->getCurrentUserId();

        $proofdata = array(
            "fname_nep"=>$this->request->fname_nep,
            "mname_nep"=>$this->request->mname_nep,
            "lname_nep"=>$this->request->lname_nep,
            "fname_eng"=>$this->request->fname_eng,
            "mname_eng"=>$this->request->mname_eng,
            "lname_eng"=>$this->request->lname_eng,
            "sex_eng"=>$this->request->sex_eng,
            "sex_nep"=>$this->request->sex_nep,
            "birth_place_eng"=>$this->request->birth_place_eng,
            "birth_place_nep"=>$this->request->birth_place_nep,
            "temp_district_id"=>$this->request->temp_district_id,
            "temp_local_level_id"=>$this->request->temp_local_level_id,
            "per_district_id"=>$this->request->per_district_id,
            "per_local_level_id"=>$this->request->per_local_level_id,
            "woda_eng"=>$this->request->woda_eng,
            "woda_nep"=>$this->request->woda_nep,
            "dob_eng"=>$this->request->dob_eng,
            "dob_nep"=>$this->request->dob_nep,
            "father_name_eng"=>$this->request->father_name_eng,
            "father_name_nep"=>$this->request->father_name_nep,
            "father_addr_eng"=>$this->request->father_addr_eng,
            "father_addr_nep"=>$this->request->father_addr_nep,
            "father_citizenship_eng"=>$this->request->father_citizenship_eng,
            "father_acitizenship_nep"=>$this->request->father_acitizenship_nep,
            "mother_name_eng"=>$this->request->mother_name_eng,
            "mother_name_nep"=>$this->request->mother_name_nep,
            "mother_addr_eng"=>$this->request->mother_addr_eng,
            "mother_addr_nep"=>$this->request->mother_addr_nep,
            "mother_citizenship_eng"=>$this->request->mother_citizenship_eng,
            "mother_acitizenship_nep"=>$this->request->mother_acitizenship_nep,
            "protect_name_nep"=>$this->request->protect_name_nep,
            "protect_name_eng"=>$this->request->protect_name_eng,
            "protect_addr_eng"=>$this->request->protect_addr_eng,
            "protect_addr_nep"=>$this->request->protect_addr_nep,
            "protect_citizenship_eng"=>$this->request->protect_citizenship_eng,
            "protect_acitizenship_nep"=>$this->request->protect_acitizenship_nep,
            "user_id"=>$user_id,
            "branch_id"=>$branch_id
        );

        $getProof = $this->minor->find($id);
        if($getProof){
            $getProof->update($proofdata);
            return $getProof;
        }else{
            return $this->minor->create($proofdata);
        }

    }

    function updateStatus($id,$status){
        $statusa = array(
            "status"=>$status
        );
        $citizench = $this->minor->find($id);
        $citizench->update($statusa);
    }
    
    
    

    function getById($id){
        return $this->minor->find($id);
    }

    function delete($id){
        return $this->minor->destroy($id);
    }


}
