<?php

namespace App\Repositories;

use App\District;

class DistrictRepository 
{
    public function __construct(
        District $district
    ){
        $this->district = $district;
    }

    public function getAll(){
        return $this->district->get();
    }

    public function getById($id){
        return $this->district->find($id);
    }

}
