<?php

namespace App\Repositories;

use App\History;
use Illuminate\Http\Request;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\UserRepository;

use App\Repositories\NewcitizenshipRepository;
use App\Repositories\CitizenshipChangeRepository;
use App\Repositories\ExforigenRepository;
use App\Repositories\ProofRepository;
use App\Repositories\ExarmyRepository;
use App\Repositories\MinorRepository;
/**
 * Class HistoryRepository.
 */
class HistoryRepository 
{
  function __construct(
    History $history,
    Request $request,
    BranchRepository $branch,
    AssignPermissionRepository $role,
    UserRepository $user,
    ServiceCenterRepository $servicecenter,
   

    // service repo
    NewcitizenshipRepository $newcitizenship,
    CitizenshipChangeRepository $citizenchange,
    ExforigenRepository $exforigen,
    ProofRepository $proof,
    ExarmyRepository $exarmy,
    MinorRepository $minor
  ){
    $this->history = $history;
    $this->request = $request;
    $this->branch = $branch;
    $this->role = $role;
    $this->user = $user;
    $this->request = $request;
    $this->servicecenter = $servicecenter;
    $this->citizenchange = $citizenchange;
    $this->newcitizenship = $newcitizenship;
    $this->exforigen = $exforigen;
    $this->proof  = $proof;
    $this->exarmy = $exarmy;
    $this->minor = $minor;
  }

  function getByCatId(){
        $type = $this->request->type;
        $catid = $this->request->catid;
        return $this->history
        ->where(array("type"=>$type,"category_id"=>$catid))
        ->take(5)
        ->orderBy('id','DESC')
        ->get();
  }

  function createHistory(){
        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        $user_id = $this->user->getCurrentUserId();

       $history = array(
        "status"=>$this->request->status,
        "description"=>$this->request->description,
        "user_id"=>$user_id,
        "branch_id"=>$branch_id,
        "type"=>$this->request->type,
        "category_id"=>$this->request->category_id
      );

      if($this->request->type == "citizenshipchange"){
        //updateStatus($id,$status)
        $this->citizenchange->updateStatus(
          $this->request->category_id,
          $this->request->status
        );
      }elseif($this->request->type == "citizenship"){
        $this->newcitizenship->updateStatus(
          $this->request->category_id,
          $this->request->status
        );
      }elseif($this->request->type == "Exforigenlist"){
          $this->exforigen->updateStatus(
            $this->request->category_id,
            $this->request->status
          );
      }elseif($this->request->type == "Prooflist"){
        $this->proof->updateStatus(
          $this->request->category_id,
          $this->request->status
        );
    }elseif($this->request->type == "Exarmylist"){
      $this->exarmy->updateStatus(
        $this->request->category_id,
        $this->request->status
      );
  }elseif($this->request->type == "MinorList"){
  $this->minor->updateStatus(
    $this->request->category_id,
    $this->request->status
  );
}



      return $this->history->create($history);
  }
}
