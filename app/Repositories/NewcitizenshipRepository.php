<?php

namespace App\Repositories;

use App\Newcitizenship;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class NewcitizenshipRepository.
 */
class NewcitizenshipRepository
{
    public function __construct(
        Request $request,
        Newcitizenship $newcitizenship,
        AssignPermissionRepository $role,
        UserRepository $user,
        ServicecenterRepository $servicecenter,
        BranchRepository $branch

    ){
        $this->request = $request;
        $this->newcitizenship = $newcitizenship;
        $this->role  = $role;
        $this->user = $user;
        $this->servicecenter = $servicecenter;
        $this->branch = $branch;
    }

    public function getAll(){
       
        if($this->role->checkRole() == "branch"){
            
            $branch = $this->branch->findBranchIdByUserId();
            return $this->newcitizenship->where("branch_id",$branch)->orderBy('id','DESC')->get();
        }else if($this->role->checkRole()=="user"){

            $userid = $this->user->getCurrentUserId();
            return $this->newcitizenship->where("user_id",$userid)->orderBy('id','DESC')->get();
        }else{
            return $this->newcitizenship->orderBy('id','DESC')->get();
        }
        
    }
    public function getById($id){
        return $this->newcitizenship->find($id);
    }

    public function getcitizenByLimit($limit){
        if($this->role->checkRole() == "branch"){
            $branch = $this->branch->findBranchIdByUserId();
            return $this->newcitizenship->where("branch_id",$branch)->take($limit)->orderBy('id','DESC')->get();
        }elseif($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->newcitizenship->where("user_id",$userid)->take($limit)->orderBy('id','DESC')->get();
        }else{
            return $this->newcitizenship->take($limit)->orderBy('id','DESC')->get();
        }
        
    }

    public function getByIdWithFormat($id){
        return $this->newcitizenship->find($id);
    }

    

    public function createUpdate($id=null){

        $this->role->checkRole();

        $branch_id =  $this->servicecenter->findBranchIdByUserId();
        
       
        $newcitizenship = array(
            "fname_eng"=>$this->request->fname_eng,
            "fname_nep"=>$this->request->fname_nep,
            "mname_eng"=>$this->request->mname_eng,
            "mname_nep"=>$this->request->mname_nep,
            "lname_eng"=>$this->request->lname_eng,
            "lname_nep"=>$this->request->lname_nep,

            "father_f_name"=>$this->request->father_f_name,
            "father_m_name"=>$this->request->father_m_name,
            "father_l_name"=>$this->request->father_l_name,
            "father_address"=>$this->request->father_address,
            "father_citizenship_id"=>$this->request->father_citizenship_id,

            "dobin_eng"=>$this->request->dobin_eng,
            "dobin_nep"=>$this->request->dobin_nep,

            "gender"=>$this->request->gender,

            "dob_district"=>$this->request->dob_district,
            "dob_municpality"=>$this->request->dob_municpality,
            "dob_wardno"=>$this->request->dob_wardno,

            "addr_district"=>$this->request->addr_district,
            "addr_municipility"=>$this->request->addr_municipility,
            "addr_wodano"=>$this->request->addr_wodano,

            "mother_name"=>$this->request->mother_name,
            "mother_address"=>$this->request->mother_address,
            "mother_citizenship_id"=>$this->request->mother_citizenship_id,

            "husband_or_wife_name"=>$this->request->husband_or_wife_name,
            "husband_or_wife_address"=>$this->request->husband_or_wife_address,
            "husband_or_wife_citizeship"=>$this->request->husband_or_wife_citizeship,

            "protector_name"=>$this->request->protector_name,
            "protector_address"=>$this->request->protector_address,
            "protector_citizeship"=>$this->request->protector_citizeship,
            "user_id"=>Auth::id(),

            "birth_place"=>$this->request->birth_place,
            "type"=>$this->request->type,

            "branch_id"=>$branch_id,

            "contact_number"=>$this->request->contact_number,
            
            "birth_id_no"=>$this->request->birth_id_no,
            "birth_issue_date"=>$this->request->birth_issue_date,
            "certificate_name"=>$this->request->certificate_name,
            "certificate_dob"=>$this->request->certificate_dob,
            "migration_issue_date"=>$this->request->migration_issue_date,
            "migration_issue_number"=>$this->request->migration_issue_number,
            "postal_ticket_no"=>$this->request->postal_ticket_no,
            "forigen_id_no"=>$this->request->forigen_id_no,
            "lived_date"=>$this->request->lived_date,

            "proof_name"=>$this->request->proof_name,
            "proof_citizen_id"=>$this->request->proof_citizen_id,
            "proof_relation"=>$this->request->proof_relation,

            "father_citi_issue"=>$this->request->father_citi_issue,
            "mother_citi_issue"=>$this->request->mother_citi_issue,
            "marriage_date"=>$this->request->marriage_date,
            "marriage_issue_date"=>$this->request->marriage_issue_date,
            "father_citi_issue_district"=>$this->request->father_citi_issue_district,
            "mother_citi_issue_district"=>$this->request->mother_citi_issue_district,
            "proof_citi_issue_district"=>$this->request->proof_citi_issue_district
        );

        $getNewcitizen = $this->newcitizenship->find($id);

        if($getNewcitizen){
           $getNewcitizen->update($newcitizenship);
           return $getNewcitizen;
        }else{
            return $this->newcitizenship->create($newcitizenship);
        }
    }

    function updateStatus($id,$status){
        $status = array(
            "status"=>$status
        );
        $citizench = $this->newcitizenship->find($id);
        $citizench->update($status);
    }


    public function delete($id){
        return $this->newcitizenship->destroy($id);
    }
}
