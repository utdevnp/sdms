<?php

namespace App\Repositories;

use App\AssignPermisson;
use App\Branch;
use App\Category;
use App\RoleHasPermission;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

/**
 * Class UserRepository.
 */
class UserRepository 
{

   public function __construct(
       User $user, 
       Request $request, 
       Auth $auth,
       Role $role,
       AssignPermisson $permission,
       Category $category,
       RoleHasPermission $rolehas,
        Branch $branch
    ){
        $this->user = $user;
        $this->request  =  $request;
        $this->auth  = $auth;
        $this->role = $role;
        $this->permission = $permission;
        $this->category = $category;
        $this->rolehas = $rolehas;
        $this->branch = $branch;
   }

   public function assignedRoles(){
        $id =  Auth::id();
        $getAssigned = $this->permission->where("user_id",$id)->orderBy('id','DESC')->get();
        $loopdata = explode(",",$getAssigned[0]->permission);
        foreach($loopdata as $role){
            $data[] = $this->category->find($role);
        }
       return $data;
   }

   public function getAll(){
       $user =  $this->user->get();
       return $user->load("assignUsers");
   }

   public function user(){
      $user =  Auth::user();
      return $user->load("assignUsers");
   }

   function getCurrentUserId(){
       return Auth::id();
   }

   public function register(){
        $this->user->name = $this->request->name;
        $this->user->email = $this->request->email;
        $this->user->password = bcrypt($this->request->password);

        return $this->user->save();
   }

   public function registerFother($name,$email,$password){
    $userreg = array(
        "name"=>$name,
        "email"=>$email,
        "password"=>bcrypt($password)
    );

    $getid =  $this->user->create($userreg);

    return $getid;
}


   public function login(){

    $credentials = request(['email', 'password']);
    if(! Auth::attempt($credentials)){
      return false;
    }else{

        $user = $this->request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($this->request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            return [
                'access_token' => $tokenResult->accessToken,
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ];
        }
   }

   public function logout(){
        $token = $this->request->user()->token();
        return $token->revoke();
   }

   public function createRole(){
        $roleName=$this->request->role_name;
        return $this->role->create(['name' => $roleName]);
   }

   public function createPermission(){
        $permission=$this->request->permission_name;
       
        $setpermiss = $this->permission->create(['name' => $permission]);
        $setpermiss->givePermissionTo($permission);

        return $setpermiss ;
    }

    public function getRoles(){
       return $this->rolehas->get();
    }

    public function assignRole($id=null){

        $permission = array(
            "permission_id" => $this->request->permission_id,
            "role_id" => $this->request->role_id,
            "user_id"=> $this->request->user_id
        );
            return $get_permission = $this->permission->where($permission);

            if($get_permission){
                return $this->permission->update($permission);
            }else{
                return $this->permission->create($permission);
            }
    }

    public function getById($id){
        return $this->user->find($id);
    }

    public function changePass($id){
        $password = array(
            "password" => bcrypt($this->request->password)
        );

        $getid = $this->user->find($id);
        if($getid){
            return $getid->update($password);
        }else{
            return false;
        }
        
    }


    function getBranchIdByUser(){
        $id =   Auth::id();
        $user_id = $this->user->find($id);
        return  $this->branch->where("user_id",$user_id->id)->get();
    }


   


}
