<?php

namespace App\Repositories;

use App\AssignPermisson;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/**
 * Class AssignPermissionRepository.
 */
class AssignPermissionRepository
{
   
    public function __construct(
        AssignPermisson $permission,
        Request $request,
        UserRepository $user
    ){
        $this->permission = $permission;
        $this->request   = $request;
        $this->user = $user;
    }

    public function assign(){
        $permission = array(
            "permission" => json_encode($this->request->permission)
        );

        $getpermis = $this->permission->find($this->request->id);
        if($getpermis){
             $getpermis->update($permission);
            return $getpermis;
        }else{
            return false;
        }
    }

    function checkRole(){
        $id =  $this->user->user()->id;
        $permission =  $this->permission->where("user_id",$id)->get();
        return @$permission[0]['user_type'];
    }
}
