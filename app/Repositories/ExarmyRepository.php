<?php

namespace App\Repositories;

use App\Exarmy;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
/**
 * Class ExarmyRepository.
 */
class ExarmyRepository
{
   
    private $exarmy;

    function __construct(
         Exarmy $exarmy,
         BranchRepository $branch,
         AssignPermissionRepository $role,
         UserRepository $user,
         Request $request,
         ServiceCenterRepository $servicecenter
    ){
        $this->exarmy = $exarmy;
        $this->branch = $branch;
        $this->role = $role;
        $this->user = $user;
        $this->request = $request;
        $this->servicecenter = $servicecenter;
    }


    function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->exarmy->findBranchIdByUserId();
            return $this->etchnic->where("branch_id",$branch)->orderBy('id','DESC')->get();
        }else if($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->exarmy->where("user_id",$userid)->orderBy('id','DESC')->get();
        }else{
            return $this->exarmy->orderBy('id','DESC')->get();
        } 
   }

   function createUpdate($id=null){

    $branch_id =  $this->servicecenter->findBranchIdByUserId();
    $user_id = $this->user->getCurrentUserId();

    $exarmydata = array(
        "name"=>$this->request->name,
        "district_id"=>$this->request->district_id,
        "local_level_id"=>$this->request->local_level_id,
        "citizenship_id"=>$this->request->citizenship_id,
        "woda"=>$this->request->woda,
        "street"=>$this->request->street,
        "relation"=>$this->request->relation,
        "patta_id"=>$this->request->patta_id,
        "ex_army_name"=>$this->request->ex_army_name,
        "pass_away_date"=>$this->request->pass_away_date,
        "famaily_mamber"=>$this->request->famaily_mamber,
        "contact_number"=>$this->request->contact_number,
        "user_id"=>$user_id,
        "branch_id"=>$branch_id
    );

    $getExarmy = $this->exarmy->find($id);
    if($getExarmy){
        $getExarmy->update($exarmydata);
        return $getExarmy;
    }else{
        return $this->exarmy->create($exarmydata);
    }

   }

   function updateStatus($id,$status){
    $statusa = array(
        "status"=>$status
    );
    $citizench = $this->exarmy->find($id);
    $citizench->update($statusa);
}




   function getById($id){
    return $this->exarmy->find($id);
}

function delete($id){
    return $this->exarmy->destroy($id);
}

}
