<?php

namespace App\Repositories;
use App\EForigenEmp;
use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
class ExforigenRepository 
{
   
    private $exforigen;

    function __construct(
         EForigenEmp $exforigen,
         BranchRepository $branch,
         AssignPermissionRepository $role,
         UserRepository $user,
         Request $request,
         ServiceCenterRepository $servicecenter
    ){
        $this->exforigen = $exforigen;
        $this->branch = $branch;
        $this->role = $role;
        $this->user = $user;
        $this->request = $request;
        $this->servicecenter = $servicecenter;
    }


    function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->exarmy->findBranchIdByUserId();
            return $this->exforigen->where("branch_id",$branch)->get();
        }else if($this->role->checkRole()=="user"){
            $userid = $this->user->getCurrentUserId();
            return $this->exforigen->where("user_id",$userid)->get();
        }else{
            return $this->exforigen->get();
        } 
   }

   function createUpdate($id=null){

    $branch_id =  $this->servicecenter->findBranchIdByUserId();
    $user_id = $this->user->getCurrentUserId();

    $empdata = array(
        "name"=>$this->request->name,
        "district_id"=>$this->request->district_id,
        "local_level_id"=>$this->request->local_level_id,
        "citizenship_id"=>$this->request->citizenship_id,
        "woda"=>$this->request->woda,
        "street"=>$this->request->street,
        "relation"=>$this->request->relation,
        "forigen_emp_name"=>$this->request->forigen_emp_name,
        "forigen_emp_country"=>$this->request->forigen_emp_country,
        "passport_no"=>$this->request->passport_no,
        "work_for"=>$this->request->work_for,
        "main_power_name"=>$this->request->main_power_name,
        "pass_away_reason"=>$this->request->pass_away_reason,
        "pass_away_date"=>$this->request->pass_away_date,
        "contact_number"=>$this->request->contact_number,
        "user_id"=>$user_id,
        "branch_id"=>$branch_id,
        "relative"=>$this->request->relative,
        "relative_citizenship"=>$this->request->relative_citizenship,
        "citizenship_issue_date"=>$this->request->citizenship_issue_date,
        "death_report_id"=>$this->request->death_report_id,
        "passport_issue_date"=>$this->request->passport_issue_date,
        "type"=>$this->request->type

    );

    $getEmp = $this->exforigen->find($id);
    if($getEmp){
        $getEmp->update($empdata);
        return $getEmp;
    }else{
        return $this->exforigen->create($empdata);
    }

   }

   function updateStatus($id,$status){
    $data = array(
        "status"=>$status
    );
    $citizench = $this->exforigen->find($id);
    $citizench->update($data);
}


   function getById($id){
        return $this->exforigen->find($id);
    }

    function delete($id){
        return $this->exforigen->destroy($id);
    }


}
