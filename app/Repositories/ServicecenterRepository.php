<?php

namespace App\Repositories;
use App\AssignPermisson;
use App\Repositories\BranchRepository;
use App\Repositories\UserRepository;
use App\Servicecenter;
use Illuminate\Http\Request;

/**
 * Class Servicecenter.
 */
class ServicecenterRepository 
{
    private $servicecenter;
    private $request;
    private $permission;
    public function __construct(
        Servicecenter $servicecenter,
        Request $request,
        UserRepository $user,
        AssignPermisson $permission,
        BranchRepository $branch,
        AssignPermissionRepository $role
    ){
        $this->servicecenter = $servicecenter;
        $this->request  = $request;
        $this->user = $user;
        $this->permission = $permission;
        $this->branch = $branch;
        $this->role = $role;
    }

    public function getAll(){
        if($this->role->checkRole() == "branch"){
            $branch = $this->branch->findBranchIdByUserId();
            return $this->servicecenter->where("branch_id",$branch)->orderBy('id','DESC')->get();
        }elseif($this->role->checkRole() == "user"){
            $userid = $this->user->getCurrentUserId();
            return $this->servicecenter->where("user_id",$userid)->orderBy('id','DESC')->get();
        }else{
            return $this->servicecenter->orderBy('id','DESC')->get();
        }
       
    }

    public function createUpdate($id = null){

        // get branch id 
        $branch_id = $this->branch->findBranchIdByUserId();
        if($id==null){
            $userCreate = $this->user->registerFother(
                $this->request->name,
                $this->request->email_address,
                ""
            );


            $userid = $userCreate->id;

            $permissonadd = array(
                "user_id"=>$userid,
                "user_type"=>"user",
                "permission"=>" "
            );

            $this->permission->create($permissonadd);

        }else{
            $userid = $this->request->user_id;
        }
        
        $servicecenter = array(
            "name" => $this->request->name,
            "email_address" => $this->request->email_address,
            "contact_number" => $this->request->contact_number,
            "district_id" => $this->request->district_id,
            "municipility_id" => $this->request->municipility_id,
            "woda_id" => $this->request->woda_id,
            "street_address" => $this->request->street_address,
            "postal_code" => $this->request->postal_code,
            "user_id" => $userid,
            "officer_name"=>$this->request->officer_name,
            "officer_phone"=>$this->request->officer_phone,
            "position"=>$this->request->position,
            "branch_id"=> $branch_id
        );

        $getServiceCenter = $this->servicecenter->find($id);

        if($getServiceCenter){
           $getServiceCenter->update($servicecenter);
           return $getServiceCenter;
        }else{
            return $this->servicecenter->create($servicecenter);
        }
    }

    public function getById($id){
       return  $this->servicecenter->find($id);
    }


    public function delete($id){
        return $this->servicecenter->destroy($id);
    }

    public function getByUserId($id){
        return Servicecenter::where("user_id",$id)->get();
    }


     // return branch id;
   public function findBranchIdByUserId(){
        $id = $this->user->getCurrentUserId();
        $servicecenter =  $this->servicecenter->where("user_id",$id)->get();
        return @$servicecenter[0]['branch_id'];
    }
}
