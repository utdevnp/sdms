<?php

namespace App\Repositories;


use App\Repositories\BranchRepository;
use App\Repositories\AssignPermissionRepository;
use App\Repositories\ServicecenterRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

use App\EForigenEmp;
use App\Newcitizenship;
use App\CitizenshipChange;
use App\Exarmy;
use App\Relief;
use App\Proof;
/**
 * Class DashboardRepository.
 */
class DashboardRepository 
{
    function __construct(
        EForigenEmp $exforigen,
        BranchRepository $branch,
        AssignPermissionRepository $role,
        UserRepository $user,
        Request $request,
        ServiceCenterRepository $servicecenter,
        Newcitizenship $newcitizenship,
        CitizenshipChange $citizenshipchenge,
        Exarmy $exarmy,
        Relief $relief,
        Proof $proof
   ){
      
       $this->branch = $branch;
       $this->role = $role;
       $this->user = $user;
       $this->request = $request;
       $this->servicecenter = $servicecenter;

       $this->newcitizenship  = $newcitizenship;
       $this->citizenshipchenge = $citizenshipchenge;
       $this->exarmy = $exarmy;
       $this->relief = $relief;
       $this->proof = $proof;
       $this->exforigen = $exforigen;
   }

   function getAll(){
    if($this->role->checkRole() == "branch"){
        $branch = $this->branch->findBranchIdByUserId();
         
        $newCitizenship  = $this->newcitizenship->where("branch_id",$branch)->count();

        $citizenshipchenge = $this->citizenshipchenge->where("branch_id",$branch)->count();
        $proof = $this->proof->where("branch_id",$branch)->count();
        $totalProof = $citizenshipchenge + $proof; 

        $exarmy = $this->exarmy->where("branch_id",$branch)->count(); 
        
        $exforigen = $this->exforigen->where("branch_id",$branch)->count();
        $relief = $this->relief->where("branch_id",$branch)->count();
        $totalRelief = $exforigen + $relief;

        return array(
            "citizenship"=>$newCitizenship,
            "proof"=>$totalProof,
            "exarmy"=>$exarmy,
            "relief"=>$totalRelief
        );

    }else if($this->role->checkRole()=="user"){
        $userid = $this->user->getCurrentUserId();
        
        $newCitizenship =  $this->newcitizenship->where("user_id",$userid)->count();

        $citizenshipchenge =  $this->citizenshipchenge->where("user_id",$userid)->count();
        $proof =  $this->proof->where("user_id",$userid)->count();
        $totalProof = $citizenshipchenge + $proof; 

        $exarmy =  $this->exarmy->where("user_id",$userid)->count();

        $exforigen =  $this->exforigen->where("user_id",$userid)->count();
        $relief =  $this->relief->where("user_id",$userid)->count();
        $totalRelief = $exforigen + $relief;
        
        return array(
            "citizenship"=>$newCitizenship,
            "proof"=>$totalProof,
            "exarmy"=>$exarmy,
            "relief"=>$totalRelief
        );

    }else{
        // new citizenship 
        $newCitizenship =  $this->newcitizenship->count();

        // Pramanit
        $citizenshipchenge = $this->citizenshipchenge->count();
        $proof =  $this->proof->count();
        $totalProof = $citizenshipchenge + $proof; 
        // bibaran bhupu
        $exarmy = $this->exarmy->count();
        // Rahat 
        $exforigen = $this->exforigen->count();
        $relief = $this->relief->count();
        $totalRelief = $exforigen + $relief;

        return array(
            "citizenship"=>$newCitizenship,
            "proof"=>$totalProof,
            "exarmy"=>$exarmy,
            "relief"=>$totalRelief
        );
    } 
   }


   function getAllPending($status){

    if($this->role->checkRole() == "branch"){
        $branch = $this->branch->findBranchIdByUserId();
         
        $newCitizenship  = $this->newcitizenship
        ->where("branch_id",$branch)
        ->where("status",$status)
        ->count();

        $citizenshipchenge = $this->citizenshipchenge
        ->where("branch_id",$branch)
        ->where("status",$status)
        ->count();

        $proof = $this->proof
        ->where("branch_id",$branch)
        ->where("status",$status)
        ->count();
        

        $exarmy = $this->exarmy
        ->where("branch_id",$branch)
        ->where("status",$status)
        ->count(); 
        
        $exforigen = $this->exforigen
        ->where("branch_id",$branch)
        ->where("status",$status)
        ->count();

        $relief = $this->relief
        ->where("branch_id",$branch)
        ->where("status",$status)
        ->count();
      

        return array(
            "citizenship"=>$newCitizenship,
            'nameage'=>$citizenshipchenge,
            "proof"=>$proof,
            "exarmy"=>$exarmy,
            "relief"=>$relief,
            "exforigen"=>$exforigen
        );

    }else if($this->role->checkRole()=="user"){
        $userid = $this->user->getCurrentUserId();
        
        $newCitizenship =  $this->newcitizenship
        ->where("user_id",$userid)
        ->where("status",$status)
        ->count();

        $citizenshipchenge =  $this->citizenshipchenge
        ->where("user_id",$userid)
        ->where("status",$status)
        ->count();

        $proof =  $this->proof
        ->where("user_id",$userid)
        ->where("status",$status)
        ->count();
       

        $exarmy =  $this->exarmy
        ->where("user_id",$userid)
        ->where("status",$status)
        ->count();

        $exforigen =  $this->exforigen
        ->where("user_id",$userid)
        ->where("status",$status)
        ->count();

        $relief =  $this->relief
        ->where("user_id",$userid)
        ->where("status",$status)
        ->count();
       
        
        return array(
            "citizenship"=>$newCitizenship,
            'nameage'=>$citizenshipchenge,
            "proof"=>$proof,
            "exarmy"=>$exarmy,
            "relief"=>$relief,
            "exforigen"=>$exforigen
        );

    }else{
        // new citizenship 
        $newCitizenship =  $this->newcitizenship
        ->where("status",$status)
        ->count();

        // Pramanit
        $citizenshipchenge = $this->citizenshipchenge
        ->where("status",$status)
        ->count();

        $proof =  $this->proof
        ->where("status",$status)
        ->count();
     
        // bibaran bhupu
        $exarmy = $this->exarmy
        ->where("status",$status)
        ->count();

        // Rahat 
        $exforigen = $this->exforigen
        ->where("status",$status)
        ->count();

        $relief = $this->relief
        ->where("status",$status)
        ->count();
       

        return array(
            "citizenship"=>$newCitizenship,
            'nameage'=>$citizenshipchenge,
            "proof"=>$proof,
            "exarmy"=>$exarmy,
            "relief"=>$relief,
            "exforigen"=>$exforigen
        );
    } 
   }



   function getAllReport($status){

    // define status
    $pending = "pending";
    $verified = "verified";
    $rejected = "rejected";

    if($this->role->checkRole() == "branch"){
        $branch = $this->branch->findBranchIdByUserId();
         
        // citizenship Report
        $newCitizenshipPending  = $this->newcitizenship
        ->where("branch_id",$branch)
        ->where("status",$pending)
        ->count();

        $newCitizenshipVerified  = $this->newcitizenship
        ->where("branch_id",$branch)
        ->where("status",$verified)
        ->count();

        $newCitizenshipRejected  = $this->newcitizenship
        ->where("branch_id",$branch)
        ->where("status",$rejected)
        ->count();


        $citizenshipchengePending = $this->citizenshipchenge
        ->where("branch_id",$branch)
        ->where("status",$pending)
        ->count();

        $citizenshipchengeVerified = $this->citizenshipchenge
        ->where("branch_id",$branch)
        ->where("status",$verified)
        ->count();

        $citizenshipchengeRejected = $this->citizenshipchenge
        ->where("branch_id",$branch)
        ->where("status",$rejected)
        ->count();

        $proofPending = $this->proof
        ->where("branch_id",$branch)
        ->where("status",$pending)
        ->count();

        $proofVerified = $this->proof
        ->where("branch_id",$branch)
        ->where("status",$verified)
        ->count();

        $proofRejected = $this->proof
        ->where("branch_id",$branch)
        ->where("status",$rejected)
        ->count();
        

        $exarmyPending = $this->exarmy
        ->where("branch_id",$branch)
        ->where("status",$pending)
        ->count();
        
        $exarmyVerified = $this->exarmy
        ->where("branch_id",$branch)
        ->where("status",$verified)
        ->count();

        $exarmyRejected = $this->exarmy
        ->where("branch_id",$branch)
        ->where("status",$rejected)
        ->count();
        
        $exforigenPending = $this->exforigen
        ->where("branch_id",$branch)
        ->where("status",$pending)
        ->count();

        $exforigenVerified = $this->exforigen
        ->where("branch_id",$branch)
        ->where("status",$verified)
        ->count();

        $exforigenRejected = $this->exforigen
        ->where("branch_id",$branch)
        ->where("status",$rejected)
        ->count();

        $reliefPending = $this->relief
        ->where("branch_id",$branch)
        ->where("status",$pending)
        ->count();

        $reliefVerified = $this->relief
        ->where("branch_id",$branch)
        ->where("status",$verified)
        ->count();

        $reliefRejected = $this->relief
        ->where("branch_id",$branch)
        ->where("status",$rejected)
        ->count();
      

        return array(
            "citizenship"=>array(
                "pending"=>$newCitizenshipPending,
                "verified"=>$newCitizenshipVerified,
                "rejected"=>$newCitizenshipRejected
            ),
            'nameage'=>array(
                "pending"=>$citizenshipchengePending,
                "verified"=>$citizenshipchengeVerified,
                "rejected"=>$citizenshipchengeRejected
            ),
            "proof"=>array(
                "pending"=>$proofPending,
                "verified"=>$proofVerified,
                "rejected"=>$proofRejected
            ),
            "exarmy"=>array(
                "pending"=>$exarmyPending,
                "verified"=>$exarmyVerified,
                "rejected"=>$exarmyRejected
            ),
            "relief"=>array(
                "pending"=>$reliefPending,
                "verified"=>$reliefVerified,
                "rejected"=>$reliefRejected
            ),
            "exforigen"=>array(
                "pending"=>$exforigenPending,
                "verified"=>$exforigenVerified,
                "rejected"=>$exforigenRejected
            )
        );

    }else if($this->role->checkRole()=="user"){
        // get user id 
        $branch = $this->user->getCurrentUserId();
        
        // citizenship Report
        $newCitizenshipPending  = $this->newcitizenship
        ->where("user_id",$branch)
        ->where("status",$pending)
        ->count();

        $newCitizenshipVerified  = $this->newcitizenship
        ->where("user_id",$branch)
        ->where("status",$verified)
        ->count();

        $newCitizenshipRejected  = $this->newcitizenship
        ->where("user_id",$branch)
        ->where("status",$rejected)
        ->count();


        $citizenshipchengePending = $this->citizenshipchenge
        ->where("user_id",$branch)
        ->where("status",$pending)
        ->count();

        $citizenshipchengeVerified = $this->citizenshipchenge
        ->where("user_id",$branch)
        ->where("status",$verified)
        ->count();

        $citizenshipchengeRejected = $this->citizenshipchenge
        ->where("user_id",$branch)
        ->where("status",$rejected)
        ->count();

        $proofPending = $this->proof
        ->where("user_id",$branch)
        ->where("status",$pending)
        ->count();

        $proofVerified = $this->proof
        ->where("user_id",$branch)
        ->where("status",$verified)
        ->count();

        $proofRejected = $this->proof
        ->where("user_id",$branch)
        ->where("status",$rejected)
        ->count();
        

        $exarmyPending = $this->exarmy
        ->where("user_id",$branch)
        ->where("status",$pending)
        ->count();
        
        $exarmyVerified = $this->exarmy
        ->where("user_id",$branch)
        ->where("status",$verified)
        ->count();

        $exarmyRejected = $this->exarmy
        ->where("user_id",$branch)
        ->where("status",$rejected)
        ->count();
        
        $exforigenPending = $this->exforigen
        ->where("user_id",$branch)
        ->where("status",$pending)
        ->count();

        $exforigenVerified = $this->exforigen
        ->where("user_id",$branch)
        ->where("status",$verified)
        ->count();

        $exforigenRejected = $this->exforigen
        ->where("user_id",$branch)
        ->where("status",$rejected)
        ->count();

        $reliefPending = $this->relief
        ->where("user_id",$branch)
        ->where("status",$pending)
        ->count();

        $reliefVerified = $this->relief
        ->where("user_id",$branch)
        ->where("status",$verified)
        ->count();

        $reliefRejected = $this->relief
        ->where("user_id",$branch)
        ->where("status",$rejected)
        ->count();

       
       
        
        return array(
            "citizenship"=>array(
                "pending"=>$newCitizenshipPending,
                "verified"=>$newCitizenshipVerified,
                "rejected"=>$newCitizenshipRejected
            ),
            'nameage'=>array(
                "pending"=>$citizenshipchengePending,
                "verified"=>$citizenshipchengeVerified,
                "rejected"=>$citizenshipchengeRejected
            ),
            "proof"=>array(
                "pending"=>$proofPending,
                "verified"=>$proofVerified,
                "rejected"=>$proofRejected
            ),
            "exarmy"=>array(
                "pending"=>$exarmyPending,
                "verified"=>$exarmyVerified,
                "rejected"=>$exarmyRejected
            ),
            "relief"=>array(
                "pending"=>$reliefPending,
                "verified"=>$reliefVerified,
                "rejected"=>$reliefRejected
            ),
            "exforigen"=>array(
                "pending"=>$exforigenPending,
                "verified"=>$exforigenVerified,
                "rejected"=>$exforigenRejected
            )
        );

    }else{
        // citizenship Report
        $newCitizenshipPending  = $this->newcitizenship
        ->where("status",$pending)
        ->count();

        $newCitizenshipVerified  = $this->newcitizenship
        ->where("status",$verified)
        ->count();

        $newCitizenshipRejected  = $this->newcitizenship
        ->where("status",$rejected)
        ->count();


        $citizenshipchengePending = $this->citizenshipchenge
        ->where("status",$pending)
        ->count();

        $citizenshipchengeVerified = $this->citizenshipchenge
        ->where("status",$verified)
        ->count();

        $citizenshipchengeRejected = $this->citizenshipchenge
        ->where("status",$rejected)
        ->count();

        $proofPending = $this->proof
        ->where("status",$pending)
        ->count();

        $proofVerified = $this->proof
        ->where("status",$verified)
        ->count();

        $proofRejected = $this->proof
        ->where("status",$rejected)
        ->count();
        

        $exarmyPending = $this->exarmy
        ->where("status",$pending)
        ->count();
        
        $exarmyVerified = $this->exarmy
        ->where("status",$verified)
        ->count();

        $exarmyRejected = $this->exarmy
        ->where("status",$rejected)
        ->count();
        
        $exforigenPending = $this->exforigen
        ->where("status",$pending)
        ->count();

        $exforigenVerified = $this->exforigen
        ->where("status",$verified)
        ->count();

        $exforigenRejected = $this->exforigen
        ->where("status",$rejected)
        ->count();

        $reliefPending = $this->relief
        ->where("status",$pending)
        ->count();

        $reliefVerified = $this->relief
        ->where("status",$verified)
        ->count();

        $reliefRejected = $this->relief
        ->where("status",$rejected)
        ->count();
       
        
        return array(
            "citizenship"=>array(
                "pending"=>$newCitizenshipPending,
                "verified"=>$newCitizenshipVerified,
                "rejected"=>$newCitizenshipRejected
            ),
            'nameage'=>array(
                "pending"=>$citizenshipchengePending,
                "verified"=>$citizenshipchengeVerified,
                "rejected"=>$citizenshipchengeRejected
            ),
            "proof"=>array(
                "pending"=>$proofPending,
                "verified"=>$proofVerified,
                "rejected"=>$proofRejected
            ),
            "exarmy"=>array(
                "pending"=>$exarmyPending,
                "verified"=>$exarmyVerified,
                "rejected"=>$exarmyRejected
            ),
            "relief"=>array(
                "pending"=>$reliefPending,
                "verified"=>$reliefVerified,
                "rejected"=>$reliefRejected
            ),
            "exforigen"=>array(
                "pending"=>$exforigenPending,
                "verified"=>$exforigenVerified,
                "rejected"=>$exforigenRejected
            )
        );
    } 
   }
   
   
}
