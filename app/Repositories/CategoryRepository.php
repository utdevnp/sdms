<?php

namespace App\Repositories;

use App\Category;
use Illuminate\Http\Request;

/**
 * Class CategoryRepository.
 */
class CategoryRepository 
{
    
    private $category;
    private $request;

    public function __construct(
        Category $category,
        Request $request
    ){
        $this->category = $category;
        $this->request = $request;
    }

    public function getAll(){
        return $this->category->where('active', 1)->get();
    }

    public function createUpdate($id=null){
        $category = array(
            "name" => $this->request->name,
            "description" => $this->request->description,
            "parent_id"=> $this->request->parent_id,
            "active" => $this->request->active,
            "slug"=> $this->request->slug
        );

        $getCategory  = $this->category->find($id);
        if($getCategory){
            $getCategory->update($category);
            return $getCategory;
        }else{
            return $this->category->create($category);
        }
    }

    public function categoryWithParent(){
        return $this->category->where('parent_id',0)->get();
    }


    public function getById($id){
        return $this->category->where('active',1)->find($id);
    }

    public function delete($id){
        return $this->category->destroy($id);
    }
}
