<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class EForigenEmp extends Model
{
    protected $fillable = [
        "name","district_id","local_level_id","citizenship_id","woda","street","relation",
        "forigen_emp_name","forigen_emp_country","passport_no","work_for","main_power_name",
        "pass_away_reason","pass_away_date","contact_number","user_id","branch_id",
        "relative","relative_citizenship","status","type","citizenship_issue_date","death_report_id",
        "passport_issue_date"
    ];


    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
}
