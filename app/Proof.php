<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Fileupload;

class Proof extends Model
{
    protected $fillable = [
        "name","district_id","local_level_id","citizenship_id","woda",
        "father_name","mother_name","grand_father_name","contact_number",
        "user_id","branch_id","type","status","husband_wife"
    ];

    protected $appends = array('proof_file');


    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }



    public function getProofFileAttribute()
    {
        return Fileupload::where("category_id",$this->attributes['id'])
        ->where("type","ProofReply")
        ->pluck('file_path')->toArray();
    }


}
