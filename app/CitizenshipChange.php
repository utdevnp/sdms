<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class CitizenshipChange extends Model
{
    protected $fillable=[
        "curent_citizenship_id","current_issue_date","full_name_eng",
        "full_name_nep","date_of_birth","issue_date","father_mother_name",
        "birth_place","user_id","branch_id","contact_number",'status'
    ];



    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['updated_at']) )->diffForHumans();
    }
}
