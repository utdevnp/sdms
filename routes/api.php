<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::post('login', 'Api\AuthController@login');
    //Route::post('signup', 'Api\AuthController@signup');
    

Route::group(['middleware' => ['cors','auth:api']], function(){
    
    Route::resource('user', 'Api\AuthController');
    Route::get('/userlist','Api\AuthController@index');
    Route::get('/getuser','Api\AuthController@getuser');
    Route::post('/logout', 'Api\AuthController@logout');
    Route::post("/chagepass/{id}","Api\AuthController@changePass");
    // roles
    Route::post("/addrole","Api\AuthController@addRole");
    Route::post("/addpermission","Api\AuthController@addPermission");
    Route::post("/getroles","Api\AuthController@getRoles");
    Route::post("/assignroles","Api\AuthController@assignRole");
    Route::post("/assign","Api\AssignPermissionController@index");
    Route::get("/getRoles","Api\AuthController@assignedRoles");

    Route::resource('branch', 'Api\BranchController');
    Route::resource('servicecenter', 'Api\ServicecenterController');
    Route::get("userbyservice/{id}",'Api\ServicecenterController@getServiceById');
    
    Route::resource('fileupload','Api\FileuploadController');
   

    Route::get("getFileByCatId/{id}","Api\FileuploadController@getByCatId");
    Route::post("filewithtype","Api\FileuploadController@getByCatIdWithType");

    Route::resource('category','Api\CategoryController');
    Route::get('categorywithparent','Api\CategoryController@categoryWithParent');

    Route::resource("new-citizenship",'Api\NewcitizenshipController');
    Route::get("citizenshiplimit/{limit}",'Api\NewcitizenshipController@getByLimit');

    Route::resource("district",'Api\DistrictController');
    Route::resource("local-level",'Api\LocallevelController');
    Route::get("getLocalByDistrict/{id}","Api\LocallevelController@getByDistrictId");

    Route::resource("changecitizenship","Api\CitizenshipChangeController");
    Route::resource("relief","Api\Reliefcontroller");
    Route::resource("history","Api\HistoryController");
    Route::post("historyBycatId","Api\HistoryController@getByCatId");
    Route::resource("proof","Api\ProofController");
    Route::resource("ethnic","Api\PrimitiveEthnicController");
    Route::resource("exarmy",'Api\ExarmyController');
    Route::resource("exforigen","Api\ExforigenController");

    // dashbaord routers
    Route::resource("dashboard","Api\DashboardController");
    Route::get("pending/{status}","Api\DashboardController@pending");
    Route::get("report/{status}","Api\DashboardController@report");

    Route::resource("minor","Api\MinorController");

});
